try:
    from itertools import zip_longest
except ImportError:
    from itertools import izip_longest as zip_longest
from fractions import Fraction
from numbertype import *
from ModP import *
from PolyMod import *

class Matrix(object):
    def __init__(self,m,n,init=True):
        if m <= 0 or n <= 0:
            raise ValueError("Invalid number of rows or columns")
        if init:
            self.values = [[0] * n for x in range(m)]
        else:
            self.values = []
        self.m = m
        self.n = n

    def __getitem__(self,idx):
        return self.values[idx]

    def __setitem__(self,idx,item):
        self.values[idx] = item

    def __str__(self):
        s = "[["
        s += ']\n ['.join([', '.join([str(item) for item in row]) for row in self.values])
        return s + ']]'

    def __repr__(self):
        s = str(self.values)
        rank = str(self.getRank())
        rep="Matrix: \"%s\", rank: \"%s\"" % (s,rank)
        return rep

    def reset(self):
        self.values = [[] for x in range(self.m)]

    def transpose(self):
        self.m, self.n = self.n, self.m
        self.values = [list(item) for item in zip(*self.values)]

    def getTranspose(self):
        m, n = self.n, self.m
        mat = Matrix(m, n)
        mat.values = [list(item) for item in zip(*self.values)]
        return mat

    def getRank(self):
        return (self.m, self.n)

    def __eq__(self, mat):
        return (mat.values == self.values)

    @classmethod
    def _makeMatrix(cls, rows):
        m = len(rows)
        n = len(rows[0])
        if any([len(row) != n for row in rows[1:]]):
            raise ValueError("Inconsistent row length")
        mat = Matrix(m,n,init=False)
        mat.values = rows
        return mat

    @classmethod
    def makeZero(cls, m, n):
        rows = [[0]*n for x in range(m)]
        return cls.fromList(rows)

    @classmethod
    def makeId(cls, m):
        rows = [[0]*m for x in range(m)]
        idx = 0
        for row in rows:
            row[idx] = 1
            idx += 1
        return cls.fromList(rows)

    @classmethod
    def fromList(cls, listoflists):
        rows = listoflists[:]
        return cls._makeMatrix(rows)

    def rowEchelonForm(self):
        d, row = 1, 0
        for k in range(min(self.m,self.n)):
            pivot = row
            while pivot < self.n and self[pivot][k] == 0:
                pivot += 1
            if pivot == self.n:
                continue
            if self[pivot] != self[row]:
                self[pivot], self[row] = self[row], self[pivot]
#            pivot = row
            c = self[pivot][k]
            for i in range(pivot+1,self.m):
                factor = self[i][k]
                self[i] = [(val1*c-factor*val2)/d for (val1, val2) in zip(self[i],self[pivot])]
            row += 1
#            d = self[pivot][k]

    def reducedRowEchelonForm(self):
        self.rowEchelonForm()
        for k in range(min(self.m,self.n)):
            for i in range(len(self[k])):
                if self[k][i] != 0:
                    break
            self[k] = [x/self[k][i] for x in self[k]]

        for i in reversed(range(self.m)):
            pivot = 0
            while pivot < self.m and self[i][pivot] == 0:
                pivot += 1
            if pivot == self.m:
                continue

            for j in range(i):
                factor = self[j][pivot]
                self[j] =[val1-factor*val2 for (val1,val2) in zip(self[j],self[pivot])]

    def modP(self,p):
        ModP = IntegersModP(p)
        polysModP = PolynomialsOver(ModP).factory
        for i in range(self.m):
            for j in range(self.n):
                print(type(self[i][j]))
                print(type(polysModP))

if __name__=="__main__":
    from fractions import Fraction
    m = Matrix(3,4)
    m[0][0] = Fraction(2,1)
    m[0][1] = Fraction(1,1)
    m[0][2] = Fraction(-1,1)
    m[1][0] = Fraction(-3,1)
    m[1][1] = Fraction(-1,1)
    m[1][2] = Fraction(2,1)
    m[2][0] = Fraction(-2,1)
    m[2][1] = Fraction(1,1)
    m[2][2] = Fraction(2,1)
    m[0][3] = Fraction(8,1)
    m[1][3] = Fraction(-11,1)
    m[2][3] = Fraction(-3,1)
#    print(m)
    m.rowEchelonForm()
    print(m)
    m.reducedRowEchelonForm()
    print(m)
    #m.modP(11)
    #ModP = IntegersModP(11)
    #polysModP = PolynomialsOver(ModP).factory
    #x = ModP(9)
    #print(isinstance(x,Polynomial))
