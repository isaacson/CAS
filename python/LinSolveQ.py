def LinSolveQ(mat):
    """ Solves a system of linear equations in Q by 
    using finite field techniques.
    Input: Matrix representing system of equations (NxM)
    Output: Solutions to system of equations
    """

