try:
    from itertools import zip_longest
except ImportError:
    from itertools import izip_longest as zip_longest
import fractions
from numbertype import *

def strip(L, elt):
    if len(L) == 0: return L

    i = len(L) -1
    while i >= 0 and L[i] == elt:
        i -= 1

    return L[:i+1]

@memoize
def PolynomialsOver(field=fractions.Fraction):

    class Polynomial(DomainElement):
        operatorPrecedence = 2

        @classmethod
        def factory(cls, L):
            return Polynomial([cls.field(x) for x in L])

        def __init__(self,c):
            if type(c) is Polynomial:
                self.coefficients = c.coefficients
            elif isinstance(c, field):
                self.coefficients = [c]
            elif not hasattr(c, '__iter__') and not hasattr(c, 'iter'):
                self.coefficients = [field(c)]
            else:
                self.coefficients = c

            self.coefficients = strip(self.coefficients, field(0))

        def isZero(self): return self.coefficients == []

        def __repr__(self):
            if self.isZero():
                return '0'

            return ' + '.join(['%s x^%d' % (a,i) if i > 0 else '%s'%a
                for i,a in enumerate(self.coefficients)])

        def __abs__(self): return len(self.coefficients)
        def __len__(self): return len(self.coefficients)
        def __sub__(self,other): return self + (-other)
        def __iter__(self): return iter(self.coefficients)
        def __neg__(self): return Polynomial([-a for a in self])
        def __call__(self,x): 
            result = 0
            for c in reversed(self.coefficients):
                result = result * x + c
            return result

        def iter(self): return self.__iter__()
        def leadingCoefficient(self): return self.coefficients[-1]
        def degree(self): return abs(self)-1

        @typecheck
        def __eq__(self,other):
            return self.degree() == other.degree() and all([x==y for (x,y) in zip(self,other)])

        def __ne__(self,other):
            return not self==other

        @typecheck
        def __add__(self,other):
            newCoefficients = [sum(x) for x in zip_longest(self,other,fillvalue=self.field(0))]
            return Polynomial(newCoefficients)

        @typecheck
        def __mul__(self,other):
            if self.isZero() or other.isZero():
                return Zero()

            newCoeffs = [self.field(0)] * (len(self) + len(other) - 1)

            for i,a in enumerate(self):
                for j,b in enumerate(other):
                    newCoeffs[i+j] += a*b

            return Polynomial(newCoeffs)

        @typecheck
        def __divmod__(self,divisor):
            q, r = Zero(), self
            divDeg = divisor.degree()
            divLC = divisor.leadingCoefficient()

            while r.degree() >= divDeg:
                monomialExponent = r.degree() - divDeg
                monomialZeros = [self.field(0)] * monomialExponent
                monomialDivisor = Polynomial(monomialZeros + [r.leadingCoefficient()/divLC])

                q += monomialDivisor
                r -= monomialDivisor*divisor

            return q, r

        @typecheck
        def __truediv__(self, divisor):
            if divisor.isZero():
                raise ZeroDivisionError
            return divmod(self,divisor)[0]

        @typecheck
        def __mod__(self, divisor):
            if divisor.isZero():
                raise ZeroDivisionError
            return divmod(self,divisor)[1]

    def Zero():
        return Polynomial([])

    Polynomial.field = field
    Polynomial.__name__ = '(%s)[x]' % field.__name__
    return Polynomial

# Testing
import unittest 
from hypothesis import given
import hypothesis.strategies as st
from ModP import *
from fractions import Fraction

class TestAddition(unittest.TestCase):
    @given(st.lists(st.integers(min_value=1)),st.lists(st.integers(min_value=1)))
    def test_addition(self,x,y):
        Mod13 = IntegersModP(13)
        polysOverQ = PolynomialsOver(Fraction).factory
        polysMod13 = PolynomialsOver(Mod13).factory
        z1 = [a + b for a,b in zip_longest(x,y,fillvalue=0)]
        z2 = [(a + b) % 13 for a,b in zip_longest(x,y,fillvalue=0)]
        self.assertEqual(polysOverQ(z1),polysOverQ(x) + polysOverQ(y))
        self.assertEqual(polysMod13(z2),polysMod13(x) + polysMod13(y))

class TestSubtraction(unittest.TestCase):
    @given(st.lists(st.integers(min_value=1)),st.lists(st.integers(min_value=1)))
    def test_subtraction(self,x,y):
        Mod13 = IntegersModP(13)
        polysOverQ = PolynomialsOver(Fraction).factory
        polysMod13 = PolynomialsOver(Mod13).factory
        y = [-a for a in y]
        z1 = [a + b for a,b in zip_longest(x,y,fillvalue=0)]
        z2 = [(a + b) % 13 for a,b in zip_longest(x,y,fillvalue=0)]
        self.assertEqual(polysOverQ(z1),polysOverQ(x) + polysOverQ(y))
        self.assertEqual(polysMod13(z2),polysMod13(x) + polysMod13(y))

class TestSubtraction(unittest.TestCase):
    @given(st.lists(st.integers(min_value=1)),st.lists(st.integers(min_value=1)))
    def test_subtraction(self,x,y):
        Mod13 = IntegersModP(13)
        polysOverQ = PolynomialsOver(Fraction).factory
        polysMod13 = PolynomialsOver(Mod13).factory
        y = [-a for a in y]
        z1 = [a + b for a,b in zip_longest(x,y,fillvalue=0)]
        z2 = [(a + b) % 13 for a,b in zip_longest(x,y,fillvalue=0)]
        self.assertEqual(polysOverQ(z1),polysOverQ(x) + polysOverQ(y))
        self.assertEqual(polysMod13(z2),polysMod13(x) + polysMod13(y))

if __name__=="__main__":
#    unittest.main()
    Mod13 = IntegersModP(13)
    polysMod13 = PolynomialsOver(Mod13).factory
    p = polysMod13([1,2,3,4,5])
    print(p(4))
