from EEA import simplify_fraction
from numbers import Number

class Fraction(object):
    def __init__(self,n,d):
        if isinstance(n,Number) and isinstance(d,Number):
            [self.num,self.den] = simplify_fraction(n,d)
        else:
            [self.num,self.den] = [n,d]
        if self.den < 0:
            self.den = -self.den
            self.num = -self.num
        elif self.den == 0:
            raise ZeroDivisionError

    def __add__(self,other):
        if isinstance(other,Number):
            return Fraction(self.num+other*self.den,self.den)
        return Fraction(self.num*other.den+self.den*other.num,self.den*other.den)

    __radd__ = __add__

    def __neg__(self):
        self.num = -self.num

    def __sub__(self,other):
        return self+-other

    def __mul__(self,other):
        if isinstance(other,Number):
            return Fraction(self.num*other,self.den)
        return Fraction(self.num*other.num,self.den*other.den)

    __rmul__ = __mul__

    def inverse(self):
        return Fraction(self.den,self.num)

    def __truediv__(self,other):
        if isinstance(other,Number):
            return Fraction(self.num,other*self.den)
        return self*other.inverse()

    __div__ = __truediv__

    def __nonzero__(self):
        if self.num != 0:
            return True
        else:
            return False

    def __str__(self):
        if self.den == 1:
            return str(self.num)
        else:
            len1 = len(str(self.num))
            len2 = len(str(self.den))
            if len1 > len2:
                string = "-"*len1 
            else:
                string = "-"*len2
            return "%s\n"%str(self.num)+string+"\n%s"%str(self.den)

    def __repr__(self):
        return "Fraction(%s,%s)"%(repr(self.num),repr(self.den))

if __name__ == "__main__":
    frac1 = Fraction(10,7)
    print(frac1)
