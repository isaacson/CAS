from EEA import inverse
from EEA import chinese_remainder
import math as m

class modular(object):
    """ Class built to store values in Z_p, and their operations
    Operations defined:
    - Addition mod n
    - Subtraction mod n
    - Multiplication mod n
    - Division mod n
    Initialization:
    a and modulus
    """
    def __init__(self,a,p):
        self.p = p
        self.a = a%p

    def __add__(self,other):
        """Addition mod n"""
        if type(other) is modular: 
            if self.p != other.p:
                raise ValueError("Incompatible modular addition")
            return modular((self.a+other.a),self.p)
        elif type(other) is int:
            return modular(self.a+other,self.p)
        else:
            raise ValueError("Modular functions only work with integers")

    def __radd__(self,other):
        """Addition on the right mod n"""
        if type(other) is modular: 
            if self.p != other.p:
                raise ValueError("Incompatible modular addition")
            return modular(self.a+other.a,self.p)
        elif type(other) is int:
            return modular(self.a+other,self.p)
        else:
            raise ValueError("Modular functions only work with integers")

    def __neg__(self):
        return self.p-self.a

    def __sub__(self,other):
        """Subtraction mod n"""
        if type(other) is modular: 
            if self.p != other.p:
                raise ValueError("Incompatible modular addition")
            a = self.a-other.a
        elif type(other) is int:
            a = (self.a-other)%self.p
        else:
            raise ValueError("Modular functions only work with integers")
        if a < 0:
            a += self.p
            a %= self.p
        return modular(a,self.p)

    def __rsub__(self,other):
        """Subtraction on the right mod n"""
        if type(other) is modular: 
            if self.p != other.p:
                raise ValueError("Incompatible modular addition")
            a = other.a-self.a
        elif type(other) is int:
            a = (other-self.a)%self.p
        else:
            raise ValueError("Modular functions only work with integers")
        if a < 0:
            a += self.p
            a %= self.p
        return modular(a,self.p)

    def __mul__(self,other):
        """Multiplication mod n"""
        if type(other) is modular: 
            if self.p != other.p:
                raise ValueError("Incompatible modular addition")
            a = (self.a*other.a)%self.p
        elif type(other) is int:
            a = (self.a*other)
        else:
            raise ValueError("Modular functions only work with integers")
        return modular(a,self.p)

    def __rmul__(self,other):
        """ Multiplication on the right mod n"""
        if type(other) is modular: 
            if self.p != other.p:
                raise ValueError("Incompatible modular addition")
            a = (self.a*other.a)%self.p
        elif type(other) is int:
            a = (self.a*other)
        else:
            raise ValueError("Modular functions only work with integers")
        return modular(a,self.p)

    def __div__(self,other):
        """Division mod n
        A helper function for division is found in EEA.py called inverse, which performs the mapping m(1/a) = b mod n.
        This mapping maps the problem from division to multiplication mod n"""
        if type(other) is modular: 
            if self.p != other.p:
                raise ValueError("Incompatible modular addition")
            a = (self.a*inverse(other.a,self.p))%self.p
        elif type(other) is int:
            a = (self.a*inverse(other,self.p))%self.p
        else:
            raise ValueError("Modular functions only work with integers")
        return modular(a,self.p)

    def __rdiv__(self,other):
        """Division on the right mod n"""
        if type(other) is modular: 
            if self.p != other.p:
                raise ValueError("Incompatible modular addition")
            a = (inverse(self.a,self.p)*other.a)%self.p
        elif type(other) is int:
            a = (inverse(self.a,self.p)*other)%self.p
        else:
            raise ValueError("Modular functions only work with integers")
        return modular(a,self.p)

    def __pow__(self,n):
        """Exponentiation mod n"""
        return pow(self.a,n,self.p)

    def __str__(self):
        """Return string of a mod n"""
        return str(self.a)

    def __repr__(self):
        return "modular(%s,%s)" % (self.a,self.p)

    def __lt__(self,other):
        """Less than definition"""
        if type(other) is modular:
            if self.p != other.p:
                raise ValueError("Incompatible modular addition")
            return self.a < other.a
        elif type(other) is int:
            return self.a < other
        else:
            raise ValueError("Modular can only be used with integers")

    def __eq__(self,other):
        """Equal to definition"""
        if type(other) is modular:
            if self.p != other.p:
                raise ValueError("Incompatible modular addition")
            return self.a == other.a
        elif type(other) is int:
            return self.a == other
        else:
            raise ValueError("Modular can only be used with integers")

    def __le__(self,other):
        """Less than equal to definition"""
        return self < other or self == other

    def __gt__(self,other):
        """Greater than definition"""
        return not self <= other

    def __ge__(self,other):
        """Greater than equal to definition"""
        return not self < other

def chinese_remainder(num):
    a = []
    n = []
    for i in num:
        a.append(i.a)
        n.append(i.p)

    sum = 0
    prod = reduce(lambda a, b: a*b, n)

    for n_i, a_i in zip(n,a):
        p = prod / n_i
        sum += a_i*inverse(p,n_i)*p

    return modular(sum,prod)

def rational_reconstruction(num):
    s = 0
    old_s = 1
    t = 1
    old_t = 0
    r = num.p
    old_r = num.a
    while abs(r) > m.sqrt(num.p):
        q = old_r / r
        [old_r, r] = [r, old_r - q*r]
        [old_s, s] = [s, old_s - q*s]
        [old_t, t] = [t, old_t - q*t]
    return [r,s]

def test():
    p = 131
    b = [modular(114,p),modular(108,p),modular(125,p),modular(1,p)]
    c = list(map(rational_reconstruction,b))

    p2 = 137
    b2 = [modular(56,p2),modular(115,p2),modular(17,p2),modular(1,p2)]
    for i in range(len(b2)):
        b2[i] = chinese_remainder([b[i],b2[i]])

    p3 = 10007
    b3 = [modular(4875,p3),modular(617,p3),modular(6772,p3),modular(1,p3)]
    for i in range(len(b2)):
        b3[i] = chinese_remainder([b2[i],b3[i]])

    c = list(map(rational_reconstruction,b3))
    for a in c:
        print(a)


if __name__ == "__main__":
    test()
