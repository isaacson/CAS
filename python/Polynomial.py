from operator import add
from itertools import izip_longest
from numbers import Number
from Fraction import Fraction
from modular import modular

class Poly(object):
    def __init__(self,*coeff,**kwargs):
        """ input: coefficients in the form a_0,...,a_n"""
        self.coeff = self.normalize(list(coeff))
        if "mod" in kwargs:
            self.mod = kwargs["mod"]
            if self.mod > 0:
                self.coeff = [x % self.mod for x in self.coeff]
        else:
            self.mod = 0

    def normalize(self,coeff):
        while coeff and coeff[-1] == 0:
            coeff.pop()
        if coeff == []:
            coeff.append(0)
        return coeff

    def degree(self):
        return len(self.coeff)

    def __str__(self):
        string = ""
        if len(self.coeff) == 1:
            return str(self.coeff[0])
        for i, coeff in enumerate(self.coeff):
            if coeff != 0:
                if i > 1:
                    string += str(coeff)+"*x^"+str(i)
                elif i == 1:
                    string += str(coeff)+"*x"
                else:
                    string += str(coeff)
                if i != len(self.coeff)-1:
                    string += "+"
            if i == len(self.coeff)-1 and coeff == 0:
                string = string[:-1]
        return string

    def __repr__(self):
        return "Polynomial("+str(self.coeff)+",mod="+str(self.mod)+")"

    def __call__(self, x):
        """ Evaluate using Horner scheme."""
        result = 0
        for c in reversed(self.coeff):
            result = result * x + c
        return result

    def __add__(self,other):
        if len(self.coeff) == 0:
            return other
        if isinstance(other, Number):
            coeff = self.coeff[:]
            coeff[0] += other
            return Poly(*coeff,mod=self.mod)
        return Poly(*[
            add(*pair)
            for pair in izip_longest(self.coeff, other.coeff, fillvalue = 0)],mod=self.mod)

    __radd__ = __add__

    def __neg__(self):
        return Poly(*[-c for c in self.coeff],mod=self.mod)

    def __sub__(self,other):
        return self + -other

    def __mul__(self,other):
        if isinstance(other,Number):
            return Poly(*[c*other for c in self.coeff],mod=self.mod)
        coeff = [0]*(len(self.coeff)+len(other.coeff))
        for i1, c1 in enumerate(self.coeff):
            for i2, c2 in enumerate(other.coeff):
                coeff[i1+i2] += c1*c2
        return Poly(*coeff,mod=self.mod)

    __rmul__ = __mul__

    def __truediv__(self,other):
        """Fast Polynomial division by using the Extended Synthetic Division algorithm.
        Also works with non-monic polynomials."""
        if isinstance(other,Number):
            return Poly(*[c / other for c in self.coeff],mod=self.mod),Poly(0,mod=self.mod)
        if len(other.coeff) == 1:
            return self/other.coeff[0]
        dividend = list(reversed(self.coeff[:]))
        divisor = list(reversed(other.coeff[:]))
        normalizer = divisor[0]
        out = dividend[:]
        for i in xrange(len(dividend)-(len(divisor)-1)):
            out[i] /= normalizer
            coeff = out[i]
            if coeff != 0:
                for j in xrange(1, len(divisor)):
                    out[i+j] += -divisor[j] * coeff
        separator = -(len(divisor)-1)
        quo = list(reversed(out[:separator]))
        rem = list(reversed(out[separator:]))
        return Poly(*quo,mod=self.mod), Fraction(Poly(*rem,mod=self.mod),other)

    __div__ = __truediv__

    def derivative(self):
        derived_coeffs = []
        exponent = 1
        for i in range(1, len(self.coeff)):
            derived_coeffs.append(self.coeff[i]*exponent)
            exponent += 1
        return Poly(*derived_coeffs)

    def __eq__(self,other):
        if isinstance(other,Number):
            if len(self.coeff) > 1:
                return False
            else:
                return self.coeff[0] == other
        elif isinstance(other,Poly):
            if len(self.coeff) != len(other.coeff):
                return False
            else:
                for i in range(len(self.coeff)):
                    if self.coeff[i] != other.coeff[i]:
                        return False
                return True

    def __ne__(self,other):
        return not self==other

    def __lt__(self,other):
        if isinstance(other,Number):
            if len(self.coeff) > 1:
                return False
            else:
                return self.coeff[0] < other
        elif isinstance(other,Poly):
            if len(self.coeff) < len(other.coeff):
                return True
            elif len(self.coeff) > len(other.coeff):
                return False
            else:
                for i in range(len(self.coeff),-1,-1):
                    if self.coeff[i] < other.coeff[i]:
                        return True
                return False

    def __ge__(self,other):
        return not self < other

    def __le__(self,other):
        return self < other or self == other

    def __gt__(self,other):
        return not self <= other

if __name__ == "__main__":
    p = Poly(293,351,143,164,mod=13)
    p2 = Poly(436,876,514,341,134,714,mod=13)
    print(p,p2)
    print(p+p2)
    print(p*p2)
    print(p-p2)
    print(p2/p)
