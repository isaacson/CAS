def extended_gcd(a,b):
    from Polynomial import Poly
    from numbers import Number
    if isinstance(a,Poly) and isinstance(b,Poly):
        r0 = a
        r = b
        s0 = Poly(1)
        s = Poly(0)
        t0 = Poly(0)
        t = Poly(1)
        i = 1
        while r != 0:
            q,rem = r0/r
            [r0,r] = [r,r0 - q*r]
            [s0,s] = [s,s0 - q*s]
            [t0,t] = [t,t0 - q*t] 
            i += 1
        g = r0
        u = s0
        v = t0
        a1 = pow(-1,i-1)*t
        b1 = pow(-1,i)*s
        return [g,u,v,a1,b1]
    elif isinstance(a,Number) and isinstance(b,Number):
        s = 0
        old_s = 1
        t = 1
        old_t = 0
        r = b
        old_r = a
        while r != 0:
            q = old_r / r
            [old_r, r] = [r, old_r - q*r]
            [old_s, s] = [s, old_s - q*s]
            [old_t, t] = [t, old_t - q*t]
        return [old_s,old_t,old_r,t,s]
    else:
        raise ValueError("GCD not defined for input type %s and %s."%(type(a),type(b)))

def simplify_fraction(num,denom):
    [old_s,old_t,old_r,t,s] = extended_gcd(num,denom)
    if s == 0: 
        raise Exception("Division by zero")
    elif s < 0:
        s = -s
        t = -t
    elif s == 1:
        return t
    return [-t,s]

def inverse(a,n):
    if a < 1:
        a = 1./a
    t0 = 0
    t = 1
    r0 = n
    r = int(a)
    while r != 0:
        q = r0 / r
        [t0, t] = [t,t0 - q*t]
        [r0, r] = [r,r0 - q*r]
    if r0 > 1: raise Exception("%d is not invertible" % a)
    if t0 < 0: t0 += n
    return t0

def chinese_remainder(n,a):
    sum = 0
    prod = reduce(lambda a, b: a*b, n)

    for n_i, a_i in zip(n,a):
        p = prod / n_i
        sum += a_i*inverse(p,n_i)*p
    return sum % prod

if __name__ == '__main__':
    from Polynomial import Poly
    from modular import modular
    p = Poly(modular(-1,13),modular(-1,13),modular(1,13),modular(1,13))
    p2 = Poly(modular(-1,13),modular(2,13),modular(3,13))
    print(extended_gcd(p2,p))
