import numpy as np

from EEA import *
from GaussianElimination import gauss

A = np.array([[1./2.,1./3.,1./4.],[1./6.,1./7.,1./8.],[1./10.,1./11.,1./12.]])
A2 = np.copy(A)
b = np.array([-1./5.,-1./9.,-1./13.])
b2 = np.copy(b)

p1 = 10007 
p2 = 76543

print (inverse(52861060,p1)*1601044627)%p1
print (-inverse(1282004,p1)*5372475)%p1
print (inverse(1156,p1)*1313)%p1

for row in range(len(A)):
    for col in range(len(A[row])):
        A[row][col] = inverse(A[row][col],p1)
        A2[row][col] = inverse(A2[row][col],p2)

for i in range(len(b)):
    b[i] = inverse(b[i],p1)
    b2[i] = inverse(b2[i],p2)

print A, b
print A2, b2

print(gauss(A,b,True,p1))
print A
print b

