from euclidean import *
from numbertype import *

class _Modular(FieldElement):
    pass

def IntegersModP(p):
    class IntegersModP(_Modular):
        def __init__(self, n):
            try:
                self.n = int(n) % IntegersModP.p
            except:
                raise TypeError("Can't cast type %s to %s in __init__" %(type(n).__name__, type(self).__name__))

            self.field = IntegersModP

        @typecheck
        def __add__(self, other): 
            return IntegersModP(self.n + other.n)

        @typecheck
        def __sub__(self, other): 
            return IntegersModP(self.n - other.n)

        @typecheck
        def __mul__(self, other): 
            return IntegersModP(self.n * other.n)

        @typecheck
        def __truediv__(self,other): 
            return self*other.inverse()

        @typecheck
        def __div__(self,other): 
            return self*other.inverse()

        def __neg__(self): 
            return IntegersModP(-self.n)

        @typecheck
        def __eq__(self,other): 
            return isinstance(other,IntegersModP) and self.n == other.n

        @typecheck
        def __ne__(self,other): 
            return not self==other

        def __abs__(self): 
            return abs(self.n)
        
        def __str__(self): 
            return str(self.n)

        def __repr__(self): 
            return '%d (mod %d)' % (self.n, self.p)

        @typecheck
        def __divmod__(self,divisor):
            q, r = divmod(self.n, divisor.n)
            return (IntegersModP(q), IntegersModP(r))

        def inverse(self):
            x,y,d = EEA(self.n,self.p)
            return IntegersModP(x)

        def __int__(self):
            return self.n

    IntegersModP.p = p
    IntegersModP.__name__ = 'Z/%d' % (p)
    return IntegersModP



