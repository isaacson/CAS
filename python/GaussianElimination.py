import numpy as np
from EEA import *

def gauss(A,b,mod=False,prime=0):
    n = len(A)
    if b.size != n:
        raise ValueError("Invalid argument")
    d = 1

    for k in range(n-1):
        # Pivot
        p = abs(A[k:,k]).argmax() + k
        if A[p, k] == 0:
            raise ValueError("Matrix is singular")

        # Swap
        if p != k:
            A[[k,p]] = A[[p, k]]
            b[[k,p]] = b[[p, k]]

        # Eliminate
        for i in range(k+1,n):
            multiplier = A[i,k]
            A[i, k:] = (A[k, k]*A[i,k:] - A[k,k:]*A[i,k])/d
            b[i] = b[i] - multiplier*b[k]
        d = A[k,k]

    for i in range(n):
        b[i] %= prime
        for j in range(n):
            A[i,j] %= prime

    x = np.zeros(n)
    for k in range(n-1,-1,-1):
        x[k] = (b[k]-np.dot(A[k,k+1:],x[k+1:]))*inverse(A[k,k],prime)%prime

    return x
