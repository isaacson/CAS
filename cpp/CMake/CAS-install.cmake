install(DIRECTORY ${CAS_BUILD_INSTALL_PREFIX}/ DESTINATION ${CMAKE_INSTALL_PREFIX})

set(CAS_INSTALL_BUILD_DIR ${CMAKE_INSTALL_PREFIX})
set(CAS_CONFIG_OUTPUT ${CMAKE_INSTALL_PREFIX}/lib/cmake/CAS/CASConfig.cmake)

configure_file(
    ${CAS_CONFIG_INPUT} ${CAS_BUILD_PREFIX}/config/exort/CASConfig_install.cmake
    @ONLY
    )

install(
    FILES
    ${CAS_BUILD_PREFIX}/config/export/CASConfig_install.cmake
    DESTINATION
    ${CMAKE_INSTALL_PREFIX}/lib/cmake/CAS/
    RENAME
    CASConfig.cmake
    )

install(
    FILES
    ${CAS_BUILD_DIR}/CASConfig-version.cmake
    DESTINATION
    ${CMAKE_INSTALL_PREFIX}/lib/cmake/CAS/
    )
