ExternalProject_Add(YAMLcpp
    DEPENDS ${_YAMLCPP_DEPENDS}
    GIT_REPOSITORY ${YAMLcpp_git}
    GIT_TAG "master"
    PREFIX ${CAS_BUILD_PREFIX}
    #UPDATE_COMMAND ${GIT_EXECUTABLE} pull
    #CONFIGURE_COMMAND ""
    #BUILD_COMMAND ""
    #INSTALL_COMMAND ""
    #DOWNLOAD_DIR ${CAS_DOWNLOAD_DIR}
    #INSTALL_DIR ${CAS_BUILD_INSTALL_PREFIX}
    #PATCH_COMMAND ${CMAKE_COMMAND}
    #    -DYAMLcpp_patch:PATH=${CAS_SOURCE_DIR}/Patches/YAMLcpp
    #    -DYAMLcpp_source:PATH=${CAS_BUILD_PREFIX}/src/YAMLcpp
    #    -P ${CAS_SOURCE_DIR}/Patches/YAMLcpp/Patch.cmake
    CMAKE_ARGS
        ${COMMON_CMAKE_ARGS}
        -DYAML_CPP_BUILD_TESTS:BOOL=OFF
)

CAS_external_project_force_install(PACKAGE YAMLcpp)

set(yaml-cpp_ROOT ${CAS_BUILD_INSTALL_PREFIX})
set(yaml-cpp_DIR ${CAS_BUILD_INSTALL_PREFIX}/lib/cmake/yaml-cpp CACHE STRING "" FORCE)

file(APPEND ${CAS_CONFIG_INPUT} "
#################################
# YAMLcpp
#################################
set(yaml-cpp_ROOT   \${CAS_ROOT})
set(yaml-cpp_DIR    \${CAS_ROOT}/lib/cmake/yaml-cpp)

set(CAS_ENABLED_YAMLCPP TRUE)
")

