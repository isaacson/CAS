ExternalProject_Add(GTest
    GIT_REPOSITORY ${GTest_git}
    GIT_TAG "master"
    PREFIX ${CAS_BUILD_PREFIX}
    #DOWNLOAD_DIR ${CAS_DOWNLOAD_DIR}
    #INSTALL_DIR ${CAS_BUILD_INSTALL_PREFIX}
    #PATCH_COMMAND ${CMAKE_COMMAND}
    #    -DGTest_patch:PATH=${CAS_SOURCE_DIR}/Patches/GTest
    #    -DGTest_source:PATH=${CAS_BUILD_PREFIX}/src/GTest
    #    -P ${CAS_SOURCE_DIR}/Patches/GTest/Patch.cmake
    CMAKE_ARGS
        ${COMMON_CMAKE_ARGS}
)

set(GTEST_ROOT ${CAS_BUILD_INSTALL_PREFIX} CACHE STRING "")

file(APPEND ${CAS_CONFIG_INPUT} "
#################################
# GTest
#################################
set(GTEST_ROOT \${CAS_ROOT})
set(CAS_ENABLED_GTest TRUE)
")
