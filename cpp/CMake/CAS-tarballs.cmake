# GTest
set(GTest_git "https://github.com/google/googletest.git")
list(APPEND CAS_external_sources GTest)

# YAMLcpp
set(YAMLcpp_git "https://github.com/jbeder/yaml-cpp.git")
list(APPEND CAS_external_sources YAMLcpp)

set(CAS_files)
foreach(source ${CAS_external_sources})
    option(CAS_ENABLE_${source} "Include ${source} version ${${source}_version}")
    set(${source}_file ${${source}_url})
endforeach()
