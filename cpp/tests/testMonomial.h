#include "gtest/gtest.h"
#include "Monomial.h"

class MonomialTest : public ::testing::Test {
    protected:
        // Set-up work for each test
        virtual void SetUp() {
            std::vector<std::string> vars1 {"x1"};
            std::vector<std::string> vars2 {"x1","x2"};
            std::vector<std::string> vars3 {"x2"};
            std::vector<std::string> vars4 {"x1","x3"};
            std::vector<std::string> vars5 {"x2","x3"};
            std::vector<std::string> vars6 {"x3"};
            std::vector<int> exp1 {2};
            std::vector<int> exp2 {1,1};
            std::vector<int> exp3 {2};
            std::vector<int> exp4 {1,1};
            std::vector<int> exp5 {1,1};
            std::vector<int> exp6 {2};
            m1 = Monomial<int>(i,vars1,exp1);
            m2 = Monomial<int>(i,vars2,exp2);
            m3 = Monomial<int>(i,vars3,exp3);
            m4 = Monomial<int>(i,vars4,exp4);
            m5 = Monomial<int>(i,vars5,exp5);
            m6 = Monomial<int>(i,vars6,exp6);
            std::vector<std::string> pvars1 {"x"};
            std::vector<std::string> pvars2 {"x","z"};
            std::vector<std::string> pvars3 {"x","y","z"};
            std::vector<std::string> pvars4 {"z"};
            std::vector<int> pexp1 {3};
            std::vector<int> pexp2 {2,2};
            std::vector<int> pexp3 {1,2,1};
            std::vector<int> pexp4 {2};
            p1 = Monomial<int>(i,pvars1,pexp1);
            p2 = Monomial<int>(i,pvars2,pexp2);
            p3 = Monomial<int>(i,pvars3,pexp3);
            p4 = Monomial<int>(i,pvars4,pexp4);
            std::vector<std::string> varsx1 {"x1","x2","x3"};
            std::vector<std::string> varsx2 {"x1","x2","x3"};
            std::vector<std::string> varsx3 {"x4","x5","x6"};
            std::vector<std::string> varsx4 {"x1","x2","x3"};
            std::vector<int> expx1 {1,1,1};
            std::vector<int> expx2 {1,1,1};
            std::vector<int> expx3 {1,1,1};
            std::vector<int> expx4 {2,3,4};
            x1 = Monomial<int>(1,varsx1,expx1);
            x2 = Monomial<int>(1,varsx2,expx2);
            x3 = Monomial<int>(1,varsx3,expx3);
            x4 = Monomial<int>(1,varsx4,expx4);
        }
        // Clean-up work for each test
        virtual void TearDown() {}

        int i = 1;
        Monomial<int> m1;
        Monomial<int> m2;
        Monomial<int> m3;
        Monomial<int> m4;
        Monomial<int> m5;
        Monomial<int> m6;
        Monomial<int> p1;
        Monomial<int> p2;
        Monomial<int> p3;
        Monomial<int> p4;
        Monomial<int> x1;
        Monomial<int> x2;
        Monomial<int> x3;
        Monomial<int> x4;
};
