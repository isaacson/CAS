#include "testRat.h"
#include <sstream>

TEST_F(RatTest, Assignment) {
    Rational r(3,4);
    r = Rational(7,9);
    EXPECT_EQ(Rational(7,9),r);
}

TEST_F(RatTest, Comparisons) {
    EXPECT_EQ(1,rat[0]);
    EXPECT_NE(1,rat[1]);
    EXPECT_LT(1,Rational(2,1));
    EXPECT_NE(rat[0],rat[1]);
    EXPECT_LT(rat[1],rat[0]);
}

TEST_F(RatTest, Increment) {
    EXPECT_EQ(Rational(3,2),Rational(1,2)++);
    EXPECT_EQ(Rational(3,2),++Rational(1,2));
    EXPECT_EQ(Rational(3,2),Rational(5,2)--);
    EXPECT_EQ(Rational(3,2),--Rational(5,2));
}

TEST_F(RatTest, DoubleConvert) {
    EXPECT_EQ(double(Rational(1,2)),0.5);
}

TEST_F(RatTest, Stream) {
    std::stringstream ss;
    ss << rat[0];
    Rational a;
    ss >> a;
    EXPECT_EQ(rat[0],a);
}

TEST_F(RatTest, Power) {
    EXPECT_EQ(rat[1].pow(4),Rational(1,16));
}

TEST_F(RatTest, IntergerOps) {
    EXPECT_EQ(Rational(5,4),1+rat[3]);
    EXPECT_EQ(Rational(3,4),1-rat[3]);
    EXPECT_EQ(Rational(1,2),2*rat[3]);
    EXPECT_EQ(Rational(8,1),2/rat[3]);
}

TEST_F(RatTest, DivisonByZero) {
    EXPECT_THROW(Rational(1,0),std::domain_error);
    Rational r = Rational(0,3);
    Rational q = Rational(3,1);
    EXPECT_THROW(q/r,std::domain_error);
    EXPECT_THROW(q/0,std::domain_error);
}

TEST_F(RatTest, NegateAndPlus) {
    EXPECT_EQ(Rational(-4,3),-Rational(4,3));
    EXPECT_EQ(Rational(4,3),+Rational(4,3));
}

TEST_F(RatTest, Simplify) {
    for(int i = 0; i < rat.size(); i=i+9) {
        EXPECT_EQ(rat[0],rat[i]);
    }
    EXPECT_EQ(rat[1],rat[11]);
    EXPECT_EQ(rat[4],rat[32]);
    EXPECT_EQ(rat[0],rat[36]);
}

TEST_F(RatTest, Add) {
    EXPECT_EQ(rat[17],rat[1]+rat[0]);
    EXPECT_EQ(Rational(0),rat[0]+rat[4]);
}
  
TEST_F(RatTest, Sub) {
    EXPECT_EQ(rat[5],rat[1]-rat[0]);
    EXPECT_EQ(Rational(2),rat[0]-rat[4]);
}

TEST_F(RatTest, Mul) {
    EXPECT_EQ(rat[1],rat[1]*rat[0]);
    EXPECT_EQ(Rational(-1),rat[0]*rat[4]);
}

TEST_F(RatTest, Div) {
    EXPECT_EQ(rat[1],rat[1]/rat[0]);
    EXPECT_EQ(Rational(-1),rat[0]/rat[4]);
}
