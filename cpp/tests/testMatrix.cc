#include "testMatrix.h"
#include "Rational.h"
#include <sstream>

TEST_F(MatTest, Constructors) {
    EXPECT_THROW(Matrix<double>({{1,2,3},{1,2}}),std::domain_error);
    Matrix<double> m{{1,2,3},{1,2,3},{1,2,3}};
    EXPECT_EQ(m,m=m);
    Matrix<double> a;
    a = m;
    EXPECT_EQ(m,a);
}

TEST_F(MatTest, Access) {
    const Matrix<double> m{{1,0,0},{0,1,0},{0,0,1}};
    EXPECT_EQ(m.at(0,0),1);
}

TEST_F(MatTest,MatOpsAdd) {
    Matrix<double> result{{11,13,15},{17,19,21},{23,25,27}};
    EXPECT_EQ(result,m1+m2);
    Matrix<double> m{{1,2},{3,4}};
    EXPECT_THROW(m+m1,std::domain_error);
}

TEST_F(MatTest,MatOpsSub) {
    Matrix<double> result{{-9,-9,-9},{-9,-9,-9},{-9,-9,-9}};
    EXPECT_EQ(result,m1-m2);
    Matrix<double> m{{1,2},{3,4}};
    EXPECT_THROW(m-m1,std::domain_error);
}

TEST_F(MatTest,MatOpsMult) {
    Matrix<double> result{{84,90,96},{201,216,231},{318,342,366}};
    EXPECT_EQ(result,m1*m2);
    Matrix<double> m{{1,2},{3,4}};
    EXPECT_THROW(m*m1,std::domain_error);
}

TEST_F(MatTest,MatOpsMultConst) {
    Matrix<double> result{{5,10,15},{20,25,30},{35,40,45}};
    EXPECT_EQ(result,x*m1);
}

TEST_F(MatTest,MatOpsDivideConst) {
    Matrix<double> result{{0.2,0.4,0.6},{0.8,1.0,1.2},{1.4,1.6,1.8}};
    EXPECT_EQ(result,m1/x);
}

TEST_F(MatTest,Negate) {
    Matrix<double> result{{-1,-2,-3},{-4,-5,-6},{-7,-8,-9}};
    EXPECT_EQ(result,-m1);
}

TEST_F(MatTest,Comparison) {
    EXPECT_EQ(m1,m1);
    EXPECT_NE(m1,m2);
}

TEST_F(MatTest,Transpose) {
    EXPECT_EQ(m1,m1.transpose().transpose());
}

TEST_F(MatTest,SwapRows) {
    Matrix<double> test = m1;
    test.swapRows(0,1);
    test.swapRows(1,0);
    EXPECT_EQ(test,m1);
}

TEST_F(MatTest,Stream) {
    std::stringstream ss;
    ss << m1;
    Matrix<double> a;
    ss >> a;
    EXPECT_EQ(m1,a);
    EXPECT_THROW(ss << "{{3,2},{2}}"; ss >> a, std::domain_error);
    Matrix<double> b;
    ss << "{}";
    ss >> a;
    EXPECT_EQ(b,a);
    EXPECT_THROW(ss << "{{3,2},"; ss >> a, std::domain_error);
    EXPECT_THROW(ss << "3,2},"; ss >> a, std::domain_error);
    EXPECT_THROW(ss << "{{3,2},}"; ss >> a, std::domain_error);
}

TEST_F(MatTest,MatRREF) {
    Matrix<Rational> m(3,4);
    m(0,0) = Rational(2,1);
    m(0,1) = Rational(1,1);
    m(0,2) = Rational(-1,1);
    m(1,0) = Rational(-3,1);
    m(1,1) = Rational(-1,1);
    m(1,2) = Rational(2,1);
    m(2,0) = Rational(-2,1);
    m(2,1) = Rational(1,1);
    m(2,2) = Rational(2,1);
    m(0,3) = Rational(8,1);
    m(1,3) = Rational(-11,1);
    m(2,3) = Rational(-3,1);

    Matrix<Rational> result(3,4);
    result(0,0) = Rational(1,1);
    result(1,1) = Rational(1,1);
    result(2,2) = Rational(1,1);
    result(0,3) = Rational(2,1);
    result(1,3) = Rational(3,1);
    result(2,3) = Rational(-1,1);

    EXPECT_EQ(result,m.reducedRowEchelon());

    Matrix<double> test1{{0.02,0.01,0,0,0.02},{1,2,1,0,1},{0,1,2,1,4},{0,0,100,200,800}};
    Matrix<double> test2{{1,0,0,0,1},{0,1,0,0,0},{0,0,1,0,0},{0,0,0,1,4}};
    EXPECT_EQ(test1.reducedRowEchelon(),test2);
}
