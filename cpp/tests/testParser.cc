#include "testParser.h"

TEST_F(ParserTest,SuccessiveArgs) {
    EXPECT_THROW(Parser("**"),ParserError);
    EXPECT_THROW(Parser("5**"),ParserError);
}

TEST_F(ParserTest,ParenErrors) {
    EXPECT_THROW(Parser(")"),ParserError);
    EXPECT_THROW(Parser("(*"),ParserError);
    EXPECT_THROW(Parser("*)"),ParserError);
    EXPECT_THROW(Parser("3("),ParserError);
}

TEST_F(ParserTest,InvalidAdjacent) {
    EXPECT_THROW(Parser("3*)"),ParserError);
    EXPECT_THROW(Parser("()"),ParserError);
    EXPECT_NO_THROW(Parser("1-("));
    EXPECT_NO_THROW(Parser("1+("));
    EXPECT_NO_THROW(Parser("1*("));
    EXPECT_NO_THROW(Parser("1/("));
    EXPECT_NO_THROW(Parser("1%("));
    EXPECT_NO_THROW(Parser("1^("));
    EXPECT_NO_THROW(Parser("(("));
    EXPECT_NO_THROW(Parser("[("));
    EXPECT_NO_THROW(Parser("{("));
    EXPECT_NO_THROW(Parser("1-["));
    EXPECT_NO_THROW(Parser("1+["));
    EXPECT_NO_THROW(Parser("1*["));
    EXPECT_NO_THROW(Parser("1/["));
    EXPECT_NO_THROW(Parser("1%["));
    EXPECT_NO_THROW(Parser("1^["));
    EXPECT_NO_THROW(Parser("(["));
    EXPECT_NO_THROW(Parser("[["));
    EXPECT_NO_THROW(Parser("{["));
    EXPECT_NO_THROW(Parser("1-{"));
    EXPECT_NO_THROW(Parser("1+{"));
    EXPECT_NO_THROW(Parser("1*{"));
    EXPECT_NO_THROW(Parser("1/{"));
    EXPECT_NO_THROW(Parser("1%{"));
    EXPECT_NO_THROW(Parser("1^{"));
    EXPECT_NO_THROW(Parser("({"));
    EXPECT_NO_THROW(Parser("[{"));
    EXPECT_NO_THROW(Parser("{{"));
    EXPECT_NO_THROW(Parser("{{3},{3}}"));
}

TEST_F(ParserTest,Tokens) {
    std::vector<std::string> tokens = {"~","3","+","4","*","y","^","(","3","*","var","%","7",")","*","~","(","x",")","-","z"};
    Parser p("-3+4*y^(3*var%7)*-(x)-z");
    for(size_t i = 0; i < tokens.size(); ++i)
        EXPECT_STREQ(tokens[i].c_str(),p.NextToken().c_str());

    std::vector<std::string> tokens2 = {"{","{","1",",","2",",","3","}",",","{","4",",","5",",","6","}","}"};
    Parser p2("{{+1,2,3},{4,5,6}}");
    for(size_t i = 0; i < tokens2.size(); ++i)
        EXPECT_STREQ(tokens2[i].c_str(),p2.NextToken().c_str());

    EXPECT_FALSE(p2.MoreTokens());
}
