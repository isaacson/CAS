#include "gtest/gtest.h"
#include "Matrix.h"

class MatTest : public ::testing::Test {
    protected:
        // Set-up work for each test
        virtual void SetUp() {}
        // Clean-up work for each test
        virtual void TearDown() {}

        const Matrix<double> m1{{1,2,3},{4,5,6},{7,8,9}},
              m2{{10,11,12},{13,14,15},{16,17,18}};
        const double x = 5;
};
