#include "testMod.h"
#include <sstream>

TEST_F(ModTest, Constructors) {
    EXPECT_NO_THROW(ModN(11));
    EXPECT_EQ(ModN(0,11),ModN(11));
    ModN a(11);
    a.SetValue(Rational(-4245,6));
    EXPECT_EQ(ModN(2,11),a);
    ModN b = ModN(3,11);
    EXPECT_EQ(b,ModN(3,11));
}

TEST_F(ModTest, Comparison) {
    EXPECT_TRUE(ModN(3,11) == ModN(3,11));
    EXPECT_TRUE(ModN(2,11) < ModN(3,11));
    EXPECT_TRUE(3 == ModN(3,11));
    EXPECT_TRUE(2 < ModN(3,11));
    Rational a(3,7);
    EXPECT_TRUE(a == ModN(2,11));
    EXPECT_TRUE(a < ModN(3,11));
}

TEST_F(ModTest, Increment) {
    EXPECT_EQ(ModN(3,11),ModN(2,11)++);
    EXPECT_EQ(ModN(3,11),++ModN(2,11));
    EXPECT_EQ(ModN(1,11),ModN(2,11)--);
    EXPECT_EQ(ModN(1,11),--ModN(2,11));
}

TEST_F(ModTest, Stream) {
    std::stringstream ss;
    ss << x[0];
    ModN a;
    ss >> a;
    EXPECT_EQ(x[0],a);
}

TEST_F(ModTest, SameMod) {
    EXPECT_THROW(x[0]+y,std::domain_error);
    EXPECT_THROW(x[0]-y,std::domain_error);
    EXPECT_THROW(x[0]*y,std::domain_error);
    EXPECT_THROW(x[0]/y,std::domain_error);
}

TEST_F(ModTest, PosNumAdd) {
    EXPECT_EQ(ModN(9,11),x[0]+x[1]);
    EXPECT_EQ(ModN(3,11),x[2]+x[3]);
    EXPECT_EQ(ModN(9,11),int64_t(4)+x[1]);
}

TEST_F(ModTest, NegNumAdd) {
    EXPECT_EQ(ModN(2,N),x[4]+x[5]);
    EXPECT_EQ(ModN(8,N),x[6]+x[7]);
}

TEST_F(ModTest, RatNumAdd) {
    EXPECT_EQ(ModN(3,N),x[8]+x[9]);
    EXPECT_EQ(ModN(0,N),x[10]+x[0]);
    Rational a(3,7);
    EXPECT_EQ(ModN(6,11),a+x[0]);
}

TEST_F(ModTest, PosNumSub) {
    EXPECT_EQ(ModN(10,11),x[0]-x[1]);
    EXPECT_EQ(ModN(1,11),x[2]-x[3]);
    EXPECT_EQ(ModN(10,11),4-x[1]);
}

TEST_F(ModTest, NegNumSub) {
    EXPECT_EQ(ModN(1,N),x[4]-x[5]);
    EXPECT_EQ(ModN(10,N),x[6]-x[7]);
}

TEST_F(ModTest, RatNumSub) {
    EXPECT_EQ(ModN(9,N),x[8]-x[9]);
    EXPECT_EQ(ModN(3,N),x[10]-x[0]);
    Rational a(3,7);
    EXPECT_EQ(ModN(9,11),a-x[0]);
}

TEST_F(ModTest, PosNumMul) {
    EXPECT_EQ(ModN(9,11),x[0]*x[1]);
    EXPECT_EQ(ModN(2,11),x[2]*x[3]);
    EXPECT_EQ(ModN(9,11),4*x[1]);
}

TEST_F(ModTest, NegNumMul) {
    EXPECT_EQ(ModN(9,N),x[4]*x[5]);
    EXPECT_EQ(ModN(2,N),x[6]*x[7]);
}

TEST_F(ModTest, RatNumMul) {
    EXPECT_EQ(ModN(4,N),x[8]*x[9]);
    EXPECT_EQ(ModN(6,N),x[10]*x[0]);
    Rational a(3,7);
    EXPECT_EQ(ModN(8,11),a*x[0]);
}

TEST_F(ModTest, PosNumDiv) {
    EXPECT_EQ(ModN(3,11),x[0]/x[1]);
    EXPECT_EQ(ModN(2,11),x[2]/x[3]);
    EXPECT_EQ(ModN(3,11),4/x[1]);
}

TEST_F(ModTest, NegNumDiv) {
    EXPECT_EQ(ModN(3,N),x[4]/x[5]);
    EXPECT_EQ(ModN(2,N),x[6]/x[7]);
}

TEST_F(ModTest, RatNumDiv) {
    EXPECT_EQ(ModN(9,N),x[8]/x[9]);
    EXPECT_EQ(ModN(10,N),x[10]/x[0]);
    Rational a(3,7);
    EXPECT_EQ(ModN(6,11),a/x[0]);
}

TEST_F(ModTest, NegateAndPlus) {
    EXPECT_EQ(ModN(8,11),-ModN(3,11));
    EXPECT_EQ(ModN(8,11),+ModN(8,11));
}

TEST_F(ModTest, Power) {
    EXPECT_EQ(ModN(3,11).pow(5),ModN(1,11));
}

TEST_F(ModTest, Inversion) {
    EXPECT_THROW(ModN(4,2).inverse(),std::domain_error);
    EXPECT_EQ(ModN(5,3).inverse(),ModN(2,3));
}

TEST_F(ModTest, ChineseRemainder) {
    std::vector<ModN> vec{x[0],y,z};
    EXPECT_EQ(ModN(12159,13871),ChineseRemainder(vec));
}

TEST_F(ModTest, TypeConversion) {
    const int64_t a = 9;
    const ModN b(9,11);
    EXPECT_EQ(9,int64_t(ModN(9,11)));
    EXPECT_EQ(a,int64_t(b));
}
