#include "testPolynomial.h"
#include <sstream>

TEST_F(PolynomialTest,Constructor) {
    Polynomial<double,lex>("-3*x+4*x^3*y+5*x-95*x*y*z-5*a+4*x*y-4*y");
    Polynomial<double,grlex>("-3*x+4*x^3*y+5*x-95*x*y*z-5*a+4*x*y-4*y");
    Polynomial<double,grevlex>("-3*x+4*x^3*y+5*x-95*x*y*z-5*a+4*x*y-4*y");
}

TEST_F(PolynomialTest,Stream) {
    Polynomial<double,lex> a("-3*x+4*x^3*y+5*x-95*x*y*z-5*a+4*x*y-4*y");
    std::stringstream ss;
    ss << a;
    Polynomial<double,lex> b;
    ss >> b;
    EXPECT_EQ(a,b);
}

TEST_F(PolynomialTest,PolyAddition) {
    Polynomial<double,lex> a("-3*x+4*y");
    Polynomial<double,lex> b("5*x+4*y+2*z");
    Polynomial<double,lex> c("2*x+8*y+2*z");
    Polynomial<double,lex> d("2*x+9*y+2*z");
    EXPECT_EQ(c,a+b);
    EXPECT_NE(d,a+b);
}

TEST_F(PolynomialTest,PolySubtraction) {
    Polynomial<double,lex> a("-3*x+4*y");
    Polynomial<double,lex> b("5*x+4*y+2*z");
    Polynomial<double,lex> c("-8*x-2*z");
    Polynomial<double,lex> d("2*x+9*y+2*z");
    EXPECT_EQ(c,a-b);
    EXPECT_NE(d,a-b);
}

TEST_F(PolynomialTest,PolyMultiplication) {
    Polynomial<double,lex> a("-3*x+4*y");
    Polynomial<double,lex> b("5*x+4*y+2*z");
    Polynomial<double,lex> c("-15*x^2+8*x*y-6*x*z+16*y^2+8*y*z");
    EXPECT_EQ(a*b,c);
}
