#include "testKinematics.h"
#include "Tools.h"
#include "yaml-cpp/yaml.h"

TEST_F(KinematicsTest,YAMLReading) {
    YAML::Node node = YAML::LoadFile("kinematics.yaml");
    Kinematics k = node["kinematics"].as<Kinematics>();
    ginac::lst incoming = k.Incoming();
    ginac::lst outgoing = k.Outgoing();
    ginac::lst testIncoming, testOutgoing;
    testIncoming.append(get_symbol("q1"));
    testIncoming.append(get_symbol("q2"));
    testOutgoing.append(get_symbol("q3"));
    EXPECT_EQ(testIncoming,incoming); 
    EXPECT_EQ(testOutgoing,outgoing); 
}
