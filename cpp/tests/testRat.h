#include "gtest/gtest.h"
#include "Rational.h"

class RatTest : public ::testing::Test {
    protected:
        // Set-up work for each test
        virtual void SetUp() {
            for(auto n : numerators) {
                for(auto d : denominators) {
                    rat.emplace_back(Rational(n,d)); 
                }
            }
        }
        // Clean-up work for each test
        virtual void TearDown() {}

        const std::vector<int> numerators{1,2,3,4,-1,-2,-3,-4};
        const std::vector<int> denominators{1,2,3,4,-1,-2,-3,-4};
        std::vector<Rational> rat;
};
