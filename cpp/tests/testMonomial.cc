#include "testMonomial.h"
#include <set>
#include <sstream>

TEST_F(MonomialTest,LexOrder) {
    std::set<Monomial<int>,lex> s;
    s.insert(p1);
    s.insert(p2);
    s.insert(p3);
    s.insert(p4);
    std::vector<Monomial<int>> m {p4,p3,p2,p1};
    std::set<Monomial<int>>::iterator it1 = s.begin();
    std::vector<Monomial<int>>::iterator it2 = m.begin();
    for(; it1 != s.end() && it2 != m.end(); ++it1, (void) ++it2) {
        EXPECT_EQ(*it1,*it2);
    }
}

TEST_F(MonomialTest,GrlexOrder) {
    std::set<Monomial<int>,grlex> s;
    s.insert(p1);
    s.insert(p2);
    s.insert(p3);
    s.insert(p4);
    std::vector<Monomial<int>> m {p4,p1,p3,p2};
    std::set<Monomial<int>>::iterator it1 = s.begin();
    std::vector<Monomial<int>>::iterator it2 = m.begin();
    for(; it1 != s.end() && it2 != m.end(); ++it1, (void) ++it2) {
        EXPECT_EQ(*it1,*it2);
    }
}

TEST_F(MonomialTest,GrevlexOrder) {
    std::set<Monomial<int>,grevlex> s;
    s.insert(p1);
    s.insert(p2);
    s.insert(p3);
    s.insert(p4);
    std::vector<Monomial<int>> m {p4,p1,p2,p3};
    std::set<Monomial<int>>::iterator it1 = s.begin();
    std::vector<Monomial<int>>::iterator it2 = m.begin();
    for(; it1 != s.end() && it2 != m.end(); ++it1, (void) ++it2) {
        EXPECT_EQ(*it1,*it2);
    }
}

TEST_F(MonomialTest,Addition) {
    std::vector<std::string> var {"x1","x2","x3"};
    std::vector<int> exp {1,1,1};
    Monomial<int> result(2,var,exp);
    EXPECT_EQ(result,x1+x2);
    EXPECT_THROW(x1+x3,std::domain_error);
    EXPECT_THROW(x1+x4,std::domain_error);
}

TEST_F(MonomialTest,Subtraction) {
    std::vector<std::string> var {"x1","x2","x3"};
    std::vector<int> exp {1,1,1};
    Monomial<int> result(0,var,exp);
    EXPECT_EQ(result,x1-x2);
    EXPECT_THROW(x1-x3,std::domain_error);
    EXPECT_THROW(x1-x4,std::domain_error);
}

TEST_F(MonomialTest,Multiplication) {
    std::vector<std::string> var {"x1","x2","x3"};
    std::vector<int> exp {2,2,2};
    Monomial<int> result(1,var,exp);
    EXPECT_EQ(result,x1*x2);
    var = {"x1","x2","x3","x4","x5","x6"};
    exp = {1,1,1,1,1,1};
    result = Monomial<int>(1,var,exp);
    EXPECT_EQ(result,x1*x3);
    var = {"x1","x2","x3"};
    exp = {3,4,5};
    result = Monomial<int>(1,var,exp);
    EXPECT_EQ(result,x1*x4);
}

TEST_F(MonomialTest,Output) {
    std::stringstream ss;
    ss << x4; 
    std::string s = "x1^2*x2^3*x3^4";
    EXPECT_STREQ(s.c_str(),ss.str().c_str()); 
}

TEST_F(MonomialTest,Eval) {
    std::map<std::string,int> point2{{"x1",1}};
    std::map<std::string,int> point{{"x1",1},{"x2",1}};
    EXPECT_THROW(m2.eval(point2),std::runtime_error);
    EXPECT_EQ(m2.eval(point),1);
    EXPECT_THROW(m4.eval(point),std::runtime_error);
}
