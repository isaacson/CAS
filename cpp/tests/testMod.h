#include "gtest/gtest.h"
#include "ModN.h"
#include "Rational.h"

class ModTest : public ::testing::Test {
    protected:
        // Set-up work for each test
        virtual void SetUp() {
            for(auto v : var1){
                ModN tmp = ModN(v,N);
                x.push_back(tmp);
            }
            for(auto v : var2) {
                ModN tmp = ModN(v,N);
                x.push_back(tmp);
            }
        }
        // Clean-up work for each test
        virtual void TearDown() {}

        const int N = 11;
        std::vector<ModN> x;
        const std::vector<int> var1{4,5,13,34,-4,-5,-13,-34};
        const std::vector<Rational> var2{Rational(1,2),Rational(5,2),Rational(76,234)};
        const ModN y = ModN(4,13);
        const ModN z = ModN(34,97);
};
