#include "ScalarProduct.h"

GINAC_IMPLEMENT_REGISTERED_CLASS_OPT(ScalarProduct, ginac::basic,
        print_func<ginac::print_context>(&ScalarProduct::do_print));

static ginac::ex make_sp(const ginac::ex& l, const ginac::ex& r) {
    return ScalarProduct(l,r);
}

REGISTER_FUNCTION(SP,eval_func(make_sp))

ScalarProduct::ScalarProduct() {
    left = 0; right = 0;
}

int ScalarProduct::compare_same_type(const ginac::basic& other) const {
    const ScalarProduct &o = static_cast<const ScalarProduct&>(other);
    if(!left.is_equal(o.left)) 
        return left.compare(o.left);
    else
        return right.compare(o.right);
}

void ScalarProduct::do_print(const ginac::print_context& c, unsigned level) const {
    c.s << "SP(" << left << ", " << right << ")";
}

static ginac::ex remove_numeric(const ginac::ex& expr, ginac::ex& prefactor) {
    if(!ginac::is_a<ginac::mul>(expr))
        return expr;
    ginac::ex result = 1;
    for(auto subexpr : expr) {
       if(ginac::is_a<ginac::numeric>(subexpr))
           prefactor *= subexpr;
       else
           result *= subexpr;
    }
    return result;
}

ginac::ex ScalarProduct::eval() const {
    ginac::ex prefactor = 1;
    ginac::ex l = left, r = right;
    l = remove_numeric(l,prefactor);
    r = remove_numeric(r,prefactor);

    if(l.compare(r) > 0)
        swap(l,r);
    if(l.is_zero() || r.is_zero())
        return 0;
    else if(l.is_equal(left) && r.is_equal(right))
        return this->hold();
    else
        return prefactor*ScalarProduct(l,r).hold();
}

ginac::ex ScalarProduct::expand(unsigned options) const {
    ginac::ex result = 0;
    ginac::ex l = left.expand(options), r = right.expand(options);

    l = ginac::is_a<ginac::add>(l) ? l : ginac::lst({l});
    r = ginac::is_a<ginac::add>(r) ? r : ginac::lst({r});

    for(auto lsub : l) {
        for(auto rsub : r) {
            result += ScalarProduct(lsub,rsub).eval();
        }
    }
    return result;
}

ginac::ex ScalarProduct::op(size_t i) const {
    if(i == 0) return left;
    else if(i == 1) return right;
    else throw std::range_error("Invalid operand"); 
}
