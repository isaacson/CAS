#include "Parser.h"
#include <sstream>
#include <algorithm>
#include <istream>
#include <iostream>

Parser::Parser(std::istream& is) {
    tokenize(std::string(std::istreambuf_iterator<char>(is),{}));
}

Parser::Parser(std::string str) {
    tokenize(str);
}

std::string Parser::NextToken() {
    std::string token = tokens.front();
    tokens.pop_front();
    return token;
}

bool Parser::MoreTokens() {
    return !tokens.empty();
}

std::vector<std::string> Parser::split(const std::string& s, const char& delim) {
    std::vector<std::string> tokens;
    std::stringstream ss(s);
    std::string token;
    while(std::getline(ss,token,delim)) {
        if(token.empty()) continue;
        tokens.push_back(token);
    }
    return tokens;
}

bool Parser::isLeftAssoc(const char& op) {
    return operators.find(op)->second.second;
}

int Parser::opPriorityDiff(const char& op1, const char& op2) {
    return (operators.find(op1)->second.first
           -operators.find(op2)->second.first);
}

bool Parser::isOperator(const char& ch) {
    if(operators.find(ch) != operators.end()) return true;
    return false;
}

bool Parser::isParen(const char& ch) {
    if(std::find(std::begin(parens),std::end(parens),ch) != std::end(parens)) return true;
    return false;
}

bool Parser::isOpenParen(const char& ch) {
    if(std::find(std::begin(parenOpen),std::end(parenOpen),ch) != std::end(parenOpen)) return true;
    return false;
}

bool Parser::isCloseParen(const char& ch) {
    if(std::find(std::begin(parenClose),std::end(parenClose),ch) != std::end(parenClose)) return true;
    return false;
}

void Parser::tokenize(std::string expr) {
    // Remove all whitespace for parsing
    expr.erase(std::remove_if(expr.begin(),expr.end(),isspace),expr.end()); 
    std::string arg;
    bool lastWasArg = false;
    for(const char ch : expr) {
        if(isOperator(ch) || isParen(ch)) {
            if(!arg.empty()) {
                if(lastWasArg) {
                    throw ParserError("Two successive arguments without an operator");
                } else if(!tokens.empty() && isCloseParen(tokens.back().front())) {
                    throw ParserError("Mismatched parenthesis. Found " 
                            + std::string{ch} 
                            + " without the matching opening parenthesis");
                }
                tokens.push_back(arg);
                arg.clear();
                lastWasArg = true;
            }
            if(tokens.empty()) {
                // Leading operator: ignore if '+', and convert unary '-' to '~'
                // Error if other operator or closing parenthesis
                if(ch == '-') {
                    tokens.push_back(std::string("~"));
                } else if(ch == '*' || ch == '/' || ch == '^' || ch == '%') {
                    throw ParserError("Invalid leading operator");
                } else if(isCloseParen(ch)) {
                    throw ParserError("Mismatched parenthesis. Found " 
                            + std::string{ch} 
                            + " without the matching opening parenthesis");
                } else {
                    tokens.push_back(std::string(1,ch));
                }
            } else if(!lastWasArg) {
                // If the previous token was also an operator/parenthesis
                auto pt = tokens.back().front();
                if(ch == '+' &&  
                   (pt == '*' || pt == '/' || pt == '^' || pt == '%' || isOpenParen(pt))) {
                    //ignore unary '+'
                } else if(ch == '-' &&  
                   (pt == '*' || pt == '/' || pt == '^' || pt == '%' || isOpenParen(pt))) {
                    tokens.push_back(std::string("~"));
                } else if((isOpenParen(ch) && !isCloseParen(pt)) ||
                          (isCloseParen(pt) && !isOpenParen(ch))) {
                    // Allowed operator pairs:
                    // +( -( *( /( %( ^( (( ([ ({ [( [[ [{ {( {[ {{
                    // )+ -) )* )/ )% )^ )) )] )} ]) ]] ]} }) }] }}
                    tokens.push_back(std::string(1,ch));
                } else {
                    throw ParserError("Invalid combination of adjacent operators");
                }
            } else {
                if(isOpenParen(ch)) {
                    throw ParserError("Opening parenthesis following an argument");
                }
                tokens.push_back(std::string(1,ch));
            }
            lastWasArg = false;
        } else {
            arg.push_back(ch);
        }
    }
    if(!arg.empty()) {
        if(lastWasArg) {
            throw ParserError("Two successive arguments without an operator");
        }
        tokens.push_back(arg);
    }
}
