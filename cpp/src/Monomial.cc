#include "Monomial.h"

std::set<std::string> compare::varList(const std::vector<std::string>& v1, const std::vector<std::string> &v2) {
    std::set<std::string> s;
    for(auto v : v1) s.insert(v);
    for(auto v : v2) s.insert(v);
    return s;
}

std::vector<int> compare::exponents(const std::map<std::string,int>& expMap, const std::set<std::string>& variables) {
    std::vector<int> result(variables.size());
    for(auto var : variables) {
        if(expMap.find(var) != expMap.end()) result.push_back(expMap.at(var));
        else result.push_back(0);
    }
    return result;
}
