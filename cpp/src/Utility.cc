#include "Utility.h"

bool Validate::pop (char closing, std::string &err) {
    if(endBrackets.find(closing) == endBrackets.end())
        return true;
    else if(brackets.empty()) {
        err = "Missing opening bracket matching to" + closing;    
        return false; 
    } else {
        char top = brackets.top();
        brackets.pop();
        if(closing != charmap.at(top)) {
            err = "Expected closing bracket " + std::string{charmap.at(top)} + " found " + closing;
            return false;
        }
        return true;
    }
}

void Validate::push(const char ch) {
    if(charmap.find(ch) != charmap.end()) brackets.push(ch);
}
