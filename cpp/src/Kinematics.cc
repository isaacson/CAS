#include <sstream>
#include "Kinematics.h"
#include "ScalarProduct.h"
#include "Tools.h"

Kinematics::Kinematics() : m_id(0), loop_prefix{"k"} {}

Kinematics& Kinematics::operator=(const Kinematics& other) {
    
}

namespace {
const char* diminc = "diminc";
const char* dimdec = "dimdec";
}

bool Kinematics::SearchDimShiftSuffix(const std::string& name, std::string& basename,
        std::string& suffix, int& shift) {
    size_t i, xlen = 6;
    int sign = 0;
    if((i = name.find(diminc)) != std::string::npos) {
        sign = 1;
    } else if((i = name.find(dimdec)) != std::string::npos) {
        sign = -1;
    } else {
        return false;
    }

    basename = name.substr(0,i);
    shift = std::stoi(name.substr(i+xlen,name.length() - i - xlen));
    shift = (sign < 0 ? - shift : shift);
    suffix = name.substr(i,name.size()-i);
    return true;
}

std::string Kinematics::DimShiftedLabel(const std::string& base, int shift) {
    std::string newbase, suffix;
    int extrashift;
    if(SearchDimShiftSuffix(base,newbase,suffix,extrashift))
        return DimShiftedLabel(newbase, shift+extrashift);
    if(shift > 0)
        return base + diminc + std::to_string(shift);
    else if(shift < 0)
        return base + dimdec + std::to_string(-shift);
    else
        return base;
}

const ginac::lst& Kinematics::External() const {
    return external;
}

const ginac::lst& Kinematics::Independent() const {
    return independent;
}

const ginac::lst& Kinematics::Incoming() const {
    return incoming;
}

const ginac::lst& Kinematics::Outgoing() const {
    return outgoing;
}

const ginac::lst& Kinematics::Invariants() const {
    return invariants;
}

const std::string Kinematics::LoopPrefix() const {
    return loop_prefix;
}

int Kinematics::id() const {
    return m_id;
}

void Kinematics::SetID(int id) {
    m_id = id;
}

bool Kinematics::operator<(const Kinematics& other) const {
    if(m_id != other.m_id)
        return m_id < other.m_id;
    return m_name < other.m_name;
}

bool Kinematics::operator==(const Kinematics& other) const {
    return !((*this) < other || other < (*this));
}

bool Kinematics::operator!=(const Kinematics& other) const {
    return !(*this == other);
}

void Kinematics::setExternal(const ginac::lst& ext) {
    external = ext;
}

void Kinematics::setIndependent(const ginac::lst& ext) {
    independent = ext;
}

void Kinematics::setIncoming(const ginac::lst& in) {
    incoming = in;
}

void Kinematics::setOutgoing(const ginac::lst& out) {
    outgoing = out;
}

void Kinematics::setInvariants(const ginac::lst& invar) {
    invariants = invar;
}

void Kinematics::setName(const std::string& name) {
    m_name = name;
}

void Kinematics::addIncoming(const std::string& in) {
    incoming.append(get_symbol(in)); 
}

void Kinematics::addOutgoing(const std::string& out) {
    outgoing.append(get_symbol(out)); 
}

void Kinematics::addExternal(const std::string& ext) {
    external.append(get_symbol(ext)); 
}

void Kinematics::addInvariant(const std::string& invar, const int& dim) {
    ginac::symbol invariant = get_symbol(invar);
    invariants.append(invariant);
    mass_dimension[invariant] = dim;
}

void Kinematics::setMomentumConservation(const std::string& s_lhs, const std::string& s_rhs) {
    ginac::ex lhs(s_lhs,external);
    ginac::ex rhs(s_rhs,external);
    if(!ginac::is_a<ginac::symbol>(lhs)) {
        std::stringstream ss;
        ss << lhs << " is not one of " << external;
        throw std::runtime_error(ss.str());
    }
    if(lhs.has(rhs))
        throw std::runtime_error("Momentum on left hand side also appears on right hand side "
                "in momentum conservation.");
    rule_momentum_conservation[lhs] = rhs;
    for(auto p : external) 
        if(p != lhs)
            independent.append(p);

    // check momenutm conservation
    ginac::ex sum = 0;
    for(auto p : incoming) sum += p;
    for(auto p : outgoing) sum -= p;
    sum = sum.subs(rule_momentum_conservation);
    if(!sum.expand().is_zero())
        throw std::runtime_error("Violation of momentum conservation for external momenta.");
}

void Kinematics::addSPRule(const std::string& p1, const std::string& p2, const std::string& invar, ginac::lst& eqns) {
    ginac::ex mom1(p1, external);
    ginac::ex mom2(p2, external);
    ginac::ex invariant(invar, invariants);
    const ginac::exmap& r = rule_momentum_conservation;
    eqns.append(ScalarProduct(mom1,mom2).subs(r).expand() == invariant);
}

void Kinematics::setSPRules(const ginac::lst& eqns) {
    ginac::exset vars;
    for(auto eqn : eqns) 
        eqn.find(ScalarProduct(ginac::wild(1),ginac::wild(2)),vars);

    int count = 0;
    ginac::lst symbols;
    ginac::exmap i2s, s2i;
    for(auto var : vars) {
        std::stringstream symbolname;
        symbolname << "tmp" << (++count);
        ginac::symbol s(symbolname.str());
        i2s[var] = s;
        s2i[s] = var;
        symbols.append(s);
    }

    ginac::ex sols = lsolve(eqns.subs(i2s),symbols,ginac::solve_algo::gauss).subs(s2i);
    ginac::exmap rules;
    try{
        if(!ginac::is_a<ginac::lst>(sols))
            throw std::runtime_error("Failed to solve the system of equations");
        ginac::lst sol = ginac::ex_to<ginac::lst>(sols);
        for(auto eq : sol) {
            if(!ginac::is_a<ginac::relational>(eq))
                throw std::runtime_error("Failed to solve system of equations");
            if(!eq.op(0).match(ScalarProduct(ginac::wild(1),ginac::wild(2))))
                throw std::runtime_error("Failed to solve system of equations");
            if(eq.op(1).has(ScalarProduct(ginac::wild(1),ginac::wild(2))))
                throw std::runtime_error("Failed to solve system of equations");
            rules[eq.op(0)] = eq.op(1);
        }
        if(rules.size() != symbols.nops())
            throw std::runtime_error("Failed to solve system of equations");
    } catch (std::exception& e) {
        std::stringstream ss;
        ss << eqns.nops() << " equations:\n" << eqns << "\n";
        ss << symbols.nops() << " unknowns:\n" << vars;
        throw std::runtime_error(std::string(e.what())+"\n"+ss.str());
    }
    rules_sp_invariants = rules;
    for(auto rule : rules_sp_invariants) {
        momenta_invariants.add(rule.first.op(0),rule.first.op(1),rule.second);
//        std::cout << "Added rule: " << rule.first.op(0) << " * " << rule.first.op(1)
//            << " --> " << rule.second << std::endl;
    }
}

void Kinematics::setSymbolReplaceOne(const std::string& var) {
    if(var == "undefined") {
        symbol_replace_one = ginac::symbol("undefined");
    } else {
        ginac::ex expr(var, invariants);
        if(!ginac::is_a<ginac::symbol>(expr))
            throw std::runtime_error("Symbol to replace by one is not a plain symbol");
        symbol_replace_one = ginac::ex_to<ginac::symbol>(expr);
    }
}
