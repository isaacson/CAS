#include "Rational.h"
#include "ModN.h"
#include <iostream>
#include <limits>

namespace {
    constexpr Int gcd(Int p, Int q){
        return q ? gcd(q, p%q) : p;
    }
}

Rational::Rational(const Rational& rat) : Rational(rat.numerator, rat.denominator) {}

Rational& Rational::simplify() {
    if(denominator < 0) {
        numerator = -numerator;
        denominator = -denominator;
    }

    const auto denom = gcd(denominator,numerator);
    numerator /= denom;
    denominator /= denom;

    return *this;
}

Rational operator+(Int a, Rational b) {
    return Rational(a) + b;
}

Rational operator-(Int a, Rational b) {
    return Rational(a) - b;
}

Rational operator*(Int a, Rational b) {
    return Rational(a) * b;
}

Rational operator/(Int a, Rational b) {
    return Rational(a) / b;
}

bool operator==(Int a, Rational b) {
    return Rational(a) == b;
}

bool operator!=(Int a, Rational b) {
    return !(Rational(a) == b);
}

bool operator<(Int a, Rational b) {
    return Rational(a) < b;
}

Rational& Rational::operator=(const Rational& rat) {
    numerator = rat.numerator;
    denominator = rat.denominator;
    return *this;
}

Rational& Rational::operator+=(const Rational& rat) {
    numerator = numerator * rat.denominator + denominator * rat.numerator;
    denominator *= rat.denominator;
    return simplify();
}

Rational& Rational::operator-=(const Rational& rat) {
    numerator = numerator * rat.denominator - denominator * rat.numerator;
    denominator *= rat.denominator;
    return simplify();
}

Rational& Rational::operator*=(const Rational& rat) {
    numerator *= rat.numerator;
    denominator *= rat.denominator;
    return simplify();
}

Rational& Rational::operator/=(const Rational& rat) {
    if(rat.numerator == 0)
        throw std::domain_error("Division by zero not allowed");
    *this *= rat.inverse();
    return simplify();
}

Rational Rational::operator+(const Rational& rat) const{
    return Rational(*this) += rat;
}

Rational Rational::operator-(const Rational& rat) const{
    return Rational(*this) -= rat;
}

Rational Rational::operator*(const Rational& rat) const{
    return Rational(*this) *= rat;
}

Rational Rational::operator/(const Rational& rat) const{
    return Rational(*this) /= rat;
}

Rational Rational::operator-() const{
    return 0 - *this;
}

Rational Rational::operator+() const{
    return *this;
}
   
bool Rational::operator==(const Rational& rat) const {
    return numerator == rat.numerator && denominator == rat.denominator;
}
   
bool Rational::operator!=(const Rational& rat) const {
    return !(*this == rat);
}

bool Rational::operator<(const Rational& rat) const{
    return numerator*rat.denominator < denominator * rat.numerator;
}

Rational Rational::operator++(int) {
    return *this += 1;
}

Rational Rational::operator--(int) {
    return *this -= 1;
}

Rational& Rational::operator++() {
    return *this += 1;
}

Rational& Rational::operator--() {
    return *this -= 1;
}

Rational::operator double() {
    return static_cast<double>(numerator)/denominator;
}

std::ostream& operator<<(std::ostream& os, const Rational& rat) {
    return os << rat.numerator << ":" << rat.denominator;
}

std::istream& operator>>(std::istream& is, Rational& rat) {
    Int p, q;
    char sep;
    if (is >> p >> sep >> q && sep == ':')
        rat = {p,q};
    return is;
}

Rational Rational::pow(unsigned exp) const {
    auto x = *this;
    Rational r(1);
    for(; exp; exp /= 2){
        if(exp%2) r *= x;
        x *= x;
    }
    return r;
}

Rational Rational::inverse() const {
    return {denominator, numerator};
}
