#include "ModN.h"
#include "Rational.h"
#include <iostream>
#include <limits>
#include <cstring>

ModN::ModN(Int N_) : N{N_}, value{0} {}

ModN::ModN(Int value_, Int N_) : N{N_}, value{value_} {
    value = value % N;
    while(value < 0)
        value += N;
}

ModN operator+(Int a, ModN b) {
    return ModN(a,b.GetMod()) + b;
}

ModN operator-(Int a, ModN b) {
    return ModN(a,b.GetMod()) - b;
}

ModN operator*(Int a, ModN b) {
    return ModN(a,b.GetMod()) * b;
}

ModN operator/(Int a, ModN b) {
    return ModN(a,b.GetMod()) / b;
}

bool operator==(Int a, ModN b) {
    return ModN(a,b.GetMod()) == b;
}

bool operator<(Int a, ModN b) {
    return ModN(a,b.GetMod()) < b;
}

ModN::ModN(Rational value_, Int N_) : N{N_} {
    value = value_.numerator;
    value *= Int(ModN(value_.denominator,N).inverse());
    value %= N;
}

void ModN::SetValue(Rational value_) {
    value = value_.numerator;
    value *= Int(ModN(value_.denominator,N).inverse());
    value %= N;
}

ModN operator+(Rational a, ModN b) {
    return ModN(a,b.GetMod()) + b;
}

ModN operator-(Rational a, ModN b) {
    return ModN(a,b.GetMod()) - b;
}

ModN operator*(Rational a, ModN b) {
    return ModN(a,b.GetMod()) * b;
}

ModN operator/(Rational a, ModN b) {
    return ModN(a,b.GetMod()) + b;
}

bool operator==(Rational a, ModN b) {
    return ModN(a,b.GetMod()) == b;
}

bool operator<(Rational a, ModN b) {
    return ModN(a,b.GetMod()) < b;
}

ModN::ModN(const ModN& mod) : N(mod.N), value(mod.value) {}

ModN& ModN::operator=(const ModN& mod) {
    value = mod.value;
    return *this;
}

ModN& ModN::operator+=(const ModN& mod) {
    if(mod.N != N) throw std::domain_error("Addition requires mod base to be the same");
    value += mod.value;
    value %= N;
    return *this;
}

ModN& ModN::operator-=(const ModN& mod) {
    if(mod.N != N) throw std::domain_error("Subtraction requires mod base to be the same");
    value -= mod.value;
    while(value < 0)
        value += N;        
    value %= N;
    return *this;
}

ModN& ModN::operator*=(const ModN& mod) {
    if(mod.N != N) throw std::domain_error("Multiplication and Division require mod base to be the same");
    value *= mod.value;
    value %= N;
    return *this;
}

ModN& ModN::operator/=(const ModN& mod) {
    return *this *= mod.inverse();
}

ModN ModN::operator+(const ModN& mod) const{
    return ModN(*this) += mod;
}

ModN ModN::operator-(const ModN& mod) const{
    return ModN(*this) -= mod;
}

ModN ModN::operator*(const ModN& mod) const{
    return ModN(*this) *= mod;
}

ModN ModN::operator/(const ModN& mod) const{
    return ModN(*this) /= mod;
}

ModN ModN::operator-() const{
    return 0-*this;
}

ModN ModN::operator+() const{
    return *this;
}

bool ModN::operator==(const ModN& mod) const{
    return value == mod.value;
}

bool ModN::operator<(const ModN& mod) const{
    return value < mod.value;
}

ModN ModN::operator++(int){
    return *this += ModN(1,N);
}

ModN ModN::operator--(int){
    return *this -= ModN(1,N);
}

ModN& ModN::operator++(){
    return *this += ModN(1,N);
}

ModN& ModN::operator--(){
    return *this -= ModN(1,N);
}

std::ostream& operator<<(std::ostream& os, const ModN& mod) {
    return os << Int(mod) << "%" << mod.GetMod();
}

std::istream& operator>>(std::istream& is, ModN& mod) {
    Int N, n;
    char sep;
    if(is >> N >> sep >> n && sep=='%') 
        mod = {N,n};
    return is;
}

ModN ModN::pow(unsigned exp) const {
    auto x = *this;
    ModN m(1,N);
    for(; exp; exp /= 2) {
        if(exp%2) m *= x;
        x *= x;
    }
    return m;
}

ModN ModN::inverse() const {
    Int t0 = 0;
    Int t = 1;
    Int r0 = N;
    Int r = value;
    while(r!=0){
        Int q = r0 / r;
        Int tmp = t0;
        t0 = t;
        t = tmp - q*t;
        tmp = r0;
        r0 = r;
        r = tmp - q*r;
    }
    if(r0 > 1)
        throw std::domain_error("Not invertible");
    if(t0 < 0)
        t0 += N;
    return ModN(t0,N);
}

ModN ChineseRemainder(std::vector<ModN> vec) {
    Int prod = 1;
    for(auto v : vec) {
        prod *= v.GetMod();
    }

    Int result = 0;

    for(auto v : vec) {
        Int pp = prod/v.GetMod();
        result += Int(v)*Int(ModN(pp,v.GetMod()).inverse())*pp;
    }

    return ModN(result,prod);
}

