#ifndef _UTILITY_H_
#define _UTILITY_H_

#include <string>
#include <sstream>
#include <set>
#include <map>
#include <stdexcept>
#include <stack>

class Convert {
    public:
        template<typename T>
        static std::string TtoString(const T &val) {
            std::ostringstream ostr;
            ostr << val;

            return ostr.str();
        };

        template<typename T>
        static T StringtoT(const std::string &val) {
            std::istringstream istr(val);
            T returnVal;
            if(!(istr >> returnVal)) 
                throw std::runtime_error("Invalid type");
            return returnVal;
        };
};

class Validate {
    public:
        const std::set<char> endBrackets{')','}',']'};
        const std::map<char,char> charmap{std::make_pair('(',')'),
            std::make_pair('{','}'),std::make_pair('[',']')};
        void push(const char);
        bool pop(char, std::string&);
        std::stack<char> brackets;
};

#endif
