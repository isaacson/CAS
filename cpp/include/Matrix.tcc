template<typename T>
Matrix<T>::Matrix(const Matrix& m) : rows_(m.rows_), cols_(m.cols_), m_data(m.m_data) {}

template<typename T>
Matrix<T>& Matrix<T>::operator=(const Matrix<T>& m) {
    if(this == &m) {
        return *this;
    }

    if(rows_ != m.rows_ || cols_ != m.cols_) {
        rows_ = m.rows_;
        cols_ = m.cols_;
    }

    m_data = m.m_data;

    return *this;
}

template<typename T>
Matrix<T>& Matrix<T>::operator+=(const Matrix<T>& m) {
    if(rows_ != m.rows_ || cols_ != m.cols_) 
        throw std::domain_error("Matrices are not compatible");
    for(size_type i = 0; i < rows_; ++i) {
        for(size_type j = 0; j < cols_; ++j) {
            m_data[index(i,j)] += m(i,j);
        }
    }

    return *this;
}

template<typename T>
Matrix<T>& Matrix<T>::operator-=(const Matrix<T>& m) {
    if(rows_ != m.rows_ || cols_ != m.cols_) 
        throw std::domain_error("Matrices are not compatible");
    for(size_type i = 0; i < rows_; ++i) {
        for(size_type j = 0; j < cols_; ++j) {
            m_data[index(i,j)] -= m(i,j);
        }
    }

    return *this;
}

template<typename T>
Matrix<T>& Matrix<T>::operator*=(const Matrix<T>& m) {
    if(cols_ != m.rows_)
        throw std::domain_error("Matrices are not compatible");
    
    // Use naive approach, may change to divide and conquer at some point if needed
    Matrix<T> temp(rows_, m.cols_);
    for(size_type i = 0; i < temp.rows_; ++i) {
        for(size_type j = 0; j < temp.cols_; ++j) {
            for(size_type k = 0; k < cols_; ++k) {
                temp(i,j) += m_data[index(i,k)]*m(k,j);
            }
        }
    }

    return (*this = temp);
}

template<typename T>
Matrix<T>& Matrix<T>::operator*=(const double d) {
    for(size_type i = 0; i < rows_; ++i) {
        for(size_type j = 0; j < cols_; ++j) {
            m_data[index(i,j)] *= d;
        }
    }

    return *this;
}

template<typename T>
Matrix<T>& Matrix<T>::operator/=(const double d) {
    for(size_type i = 0; i < rows_; ++i) {
        for(size_type j = 0; j < cols_; ++j) {
            m_data[index(i,j)] /= d;
        }
    }

    return *this;
}

template<typename T>
Matrix<T> Matrix<T>::operator+(const Matrix<T>& m) const {
    return Matrix<T>(*this) += m;
}

template<typename T>
Matrix<T> Matrix<T>::operator-(const Matrix<T>& m) const {
    return Matrix<T>(*this) -= m;
}

template<typename T>
Matrix<T> Matrix<T>::operator*(const Matrix<T>& m) const {
    return Matrix<T>(*this) *= m;
}

template<typename T>
Matrix<T> Matrix<T>::operator*(const double d) const {
    return Matrix<T>(*this) *= d;
}

template<typename T>
Matrix<T> operator*(const double d, const Matrix<T> m) {
    return m * d;
}

template<typename T>
Matrix<T> Matrix<T>::operator/(const double d) const {
    return Matrix<T>(*this) /= d;
}

template<typename T>
Matrix<T> Matrix<T>::operator+() const {
    return Matrix<T>(*this);
}

template<typename T>
Matrix<T> Matrix<T>::operator-() const {
    return Matrix<T>(*this)*-1;
}

template<typename T>
bool Matrix<T>::operator==(const Matrix<T>& m) const {
    return rows_ == m.rows_ && cols_ == m.cols_ && m_data == m.m_data;
}

template<typename T>
std::ostream& operator<< (std::ostream& os, const Matrix<T>& m) {
    os << "{";
    for(auto i = 0; i < m.rows_; ++i) {
        if(i != 0) os << " ";
        os << "{";
        for(auto j = 0; j < m.cols_; ++j) {
            os << m.at(i,j);
            if(j < m.cols_-1) os << ", ";
        }
        if(i < m.rows_-1)
            os << "},\n";
        else
            os << "}}";
    }

    return os;
}

template<typename T>
std::vector<T> ParseRows(std::vector<std::string> rows, int &nrows, int &ncols) {
    std::vector<T> result;
    for(auto row : rows) {
        int this_col = 0;
        row = row.substr(1,row.size()-2);
        size_t pos = 0;
        while((pos = row.find(',')) != std::string::npos) {
            result.push_back(Convert::StringtoT<T>(row.substr(0,pos)));
            row.erase(0, pos+1);
            this_col++;
            if(nrows == 0) ncols++;
        }
        result.push_back(Convert::StringtoT<T>(row));
        this_col++;
        if(nrows == 0) ncols++;
        nrows++; 
        if(this_col != ncols) 
            throw std::domain_error("Number of columns in row " + std::to_string(nrows) 
                    + " does not equal the number of columns in the first row.");
    }

    return result;
}

template<typename T>
std::istream& operator>> (std::istream& is, Matrix<T>& m) {
    Validate valid;
    std::string mat(std::istreambuf_iterator<char>(is),{}); 
    mat.erase(std::remove(mat.begin(),mat.end(),'\n'),mat.end());
    mat.erase(std::remove_if(mat.begin(),mat.end(),isspace),mat.end());
    std::string err;
    for(auto ch : mat) {
        valid.push(ch);
        bool isValid = valid.pop(ch,err);
        if(!isValid) throw std::domain_error("Invalid input for matrix. Error is:" + err);
    }
    if(valid.brackets.size() != 0) throw std::domain_error("Missing closing brackets");
    if(mat.size() == 2) {
        m = Matrix<T>();
        return is;
    }
    mat = mat.substr(1,mat.size()-2);
    if(mat[mat.size()-1] == ',') 
        throw std::domain_error("Invalid input for matrix. Expecting \"}\" at end, but found \",\"");
    size_t pos = 0;
    std::vector<std::string> rows;
    while((pos = mat.find('}')) != std::string::npos) {
        rows.push_back(mat.substr(0,pos+1));
        mat.erase(0, pos+2);
    }
    int nrows = 0, ncols=0;
    std::vector<T> result = ParseRows<T>(rows,nrows,ncols);
    m = Matrix<T>(nrows,ncols,result);

    return is;
}

//template<typename T>
//Matrix<T> Matrix<T>::pow(unsigned exp) const {
//
//}

template<typename T>
void Matrix<T>::swapRows(size_type i, size_type j) {
    std::vector<T> temp = m_data;
    for(size_type k = 0; k < cols_; ++k) {
        m_data[index(i,k)] = temp[index(j,k)];
        m_data[index(j,k)] = temp[index(i,k)];
    }
}

template<typename T>
Matrix<T> Matrix<T>::transpose() const {
    Matrix<T> m(cols_,rows_);
    for(size_type i = 0; i < rows_; ++i) {
        for(size_type j = 0; j < cols_; ++j) {
            m(j,i) = m_data[index(i,j)];
        }
    }

    return m;
}
    
template<typename T>
Matrix<T> Matrix<T>::augment(const Matrix<T> m) {
    if(rows_ != m.rows_) 
        throw std::domain_error("Incompatible matrices for creating an aumented matrix");
    Matrix ret(rows_, cols_ + m.cols_);
    for(size_type i = 0; i < ret.rows_; ++i) {
        for(size_type j = 0; j < ret.cols_; ++j) {
            if(j < cols_)
                ret(i,j) = m_data[index(i,j)];
            else
                ret(i,j) = m(i,j-cols_);
        }
    }

    return ret;
}

template<typename T>
Matrix<T> Matrix<T>::rowEchelon() {
    Matrix<T> m = (*this);
    T d{1};
    size_type row = 0;
    for(size_type k = 0; k < std::min(rows_,cols_); ++k) {
        size_type pivot = row;
        while(pivot < rows_ && m(pivot,k) == 0)
            pivot += 1;
        if(pivot == rows_)
            continue;
        Row r_pivot(*this,pivot), r_row(*this,row);
        if(r_pivot != r_row)
            swapRows(pivot,row);
        T c = m(pivot,k);
        for(size_type i = pivot+1; i < rows_; ++i) {
            T factor = m(i,k);
            for(size_type j = 0; j < cols_; ++j) {
                m(i,j) = (m(i,j)*c-factor*m(pivot,j))/d;
            }
        }
        row += 1;
        d = m(pivot,k);
    }

    return m;
}

template<typename T>
Matrix<T> Matrix<T>::reducedRowEchelon() {
    Matrix<T> m = rowEchelon();
    for(size_type k = 0; k < std::min(rows_,cols_); ++k) {
        T val{0};
        for(size_type i = 0; i <= k; ++i) {
            if(m(k,i) != 0) {
                val = m(k,i);
                break;
            }
        }
        for(size_type i = 0; i < cols_; ++i) {
            m(k,i) = m(k,i)/val;
        }
    }

    for(size_type i = rows_-1; i > 0; --i) {
        size_type pivot = 0;
        while(pivot < rows_ && m(i,pivot) == 0)
            pivot += 1;
        if(pivot==rows_) continue;

        for(size_type j = 0; j < i; ++j) {
            T factor = m(j,pivot);
            for(size_type k = 0; k < cols_; ++k) {
                m(j,k) = m(j,k)-factor*m(pivot,k);
            }
        }
    }

    return m;
}

template<typename T>
Matrix<T> Matrix<T>::inverse() {

}
