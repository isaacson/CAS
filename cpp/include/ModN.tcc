template<Int N>
Mod<N>::operator Int() {
    return static_cast<Int>(value);
}

template<Int N>
Mod<N>::Mod(Int value_) : value{value_} {
    value = value % N;
}

template<Int N>
constexpr Mod<N> operator+(Int a, Mod<N> b) {
    return Mod<N>(a) + b;
}

template<Int N>
constexpr Mod<N> operator-(Int a, Mod<N> b) {
    return Mod<N>(a) - b;
}

template<Int N>
constexpr Mod<N> operator*(Int a, Mod<N> b) {
    return Mod<N>(a) * b;
}

template<Int N>
constexpr Mod<N> operator/(Int a, Mod<N> b) {
    return Mod<N>(a) + b;
}

template<Int N>
constexpr bool operator==(Int a, Mod<N> b) {
    return Mod<N>(a) == b;
}

template<Int N>
constexpr bool operator<(Int a, Mod<N> b) {
    return Mod<N>(a) < b;
}

template<Int N>
Mod<N>::Mod(Rational value_) {
    value = value_.numerator;
    std::cout << Mod<N>(value_.denominator) << std::endl;
    value *= Int(Mod<N>(value_.denominator).inverse());
    value %= N;
}

template<Int N>
constexpr Mod<N> operator+(Rational a, Mod<N> b) {
    return Mod<N>(a) + b;
}

template<Int N>
constexpr Mod<N> operator-(Rational a, Mod<N> b) {
    return Mod<N>(a) - b;
}

template<Int N>
constexpr Mod<N> operator*(Rational a, Mod<N> b) {
    return Mod<N>(a) * b;
}

template<Int N>
constexpr Mod<N> operator/(Rational a, Mod<N> b) {
    return Mod<N>(a) + b;
}

template<Int N>
constexpr Mod<N> operator==(Rational a, Mod<N> b) {
    return Mod<N>(a) == b;
}

template<Int N>
constexpr Mod<N> operator<(Rational a, Mod<N> b) {
    return Mod<N>(a) < b;
}

template<Int N>
Mod<N>::Mod(const Mod& mod) : value(mod.value) {}

template<Int N>
Mod<N>& Mod<N>::operator=(const Mod<N>& mod) {
    value = mod.value;
    return *this;
}

template<Int N>
Mod<N>& Mod<N>::operator+=(const Mod<N>& mod) {
    value += mod.value;
    value %= N;
    return *this;
}

template<Int N>
Mod<N>& Mod<N>::operator-=(const Mod<N>& mod) {
    value -= mod.value;
    while(value < 0)
        value += N;        
    value %= N;
    return *this;
}

template<Int N>
Mod<N>& Mod<N>::operator*=(const Mod<N>& mod) {
    value *= mod.value;
    value %= N;
    return *this;
}

template<Int N>
Mod<N>& Mod<N>::operator/=(const Mod<N>& mod) {
    return *this *= mod.inverse();
}

template<Int N>
Mod<N> Mod<N>::operator+(const Mod<N>& mod) const{
    return Mod<N>(*this) += mod;
}

template<Int N>
Mod<N> Mod<N>::operator-(const Mod<N>& mod) const{
    return Mod<N>(*this) -= mod;
}

template<Int N>
Mod<N> Mod<N>::operator*(const Mod<N>& mod) const{
    return Mod<N>(*this) *= mod;
}

template<Int N>
Mod<N> Mod<N>::operator/(const Mod<N>& mod) const{
    return Mod<N>(*this) /= mod;
}

template<Int N>
Mod<N> Mod<N>::operator-() const{
    return 0-*this;
}

template<Int N>
Mod<N> Mod<N>::operator+() const{
    return *this;
}

template<Int N>
bool Mod<N>::operator==(const Mod<N>& mod) const{
    return value == mod.value;
}

template<Int N>
bool Mod<N>::operator<(const Mod<N>& mod) const{
    return value < mod.value;
}

template<Int N>
Mod<N> Mod<N>::operator++(int){
    return *this += 1;
}

template<Int N>
Mod<N> Mod<N>::operator--(int){
    return *this -= 1;
}

template<Int N>
Mod<N>& Mod<N>::operator++(){
    return *this += 1;
}

template<Int N>
Mod<N>& Mod<N>::operator--(){
    return *this -= 1;
}

template<Int N>
std::ostream& operator<<(std::ostream& os, const Mod<N>& mod) {
    return os << mod.value << " mod " << N;
}

template<Int N>
Mod<N> Mod<N>::pow(unsigned exp) const {
    auto x = *this;
    Mod<N> m(1);
    for(; exp; exp /= 2) {
        if(exp%2) m *= x;
        x *= x;
    }
    return m;
}

template<Int N>
Mod<N> Mod<N>::inverse() const {
    Int t0 = 0;
    Int t = 1;
    Int r0 = N;
    Int r = value;
    std::cout << r << "  " << N << std::endl;
    while(r!=0){
        Int q = r0 / r;
        Int tmp = t0;
        t0 = t;
        t = tmp - q*t;
        tmp = r0;
        r0 = r;
        r = tmp - q*r;
    }
    if(r0 > 1)
        throw std::domain_error("Not invertible");
    if(t0 < 0)
        t0 += N;
    return Mod(t0);
}

