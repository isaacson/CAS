#ifndef _PARSER_H_
#define _PARSER_H_

#include <string>
#include <vector>
#include <deque>
#include <unordered_map>
#include <map>
#include <queue>
#include <exception>
#include <iosfwd>

class ParserError : public std::exception {
    public:
        ParserError(std::string msg) {
            message = "ParserError: " + msg;
        }
        virtual const char* what() const throw() {
            return message.c_str(); 
        }
    private:
        std::string message;
};

class Parser {
    using OpMap = typename std::map<char,std::pair<int,bool>>;
    using Paren = typename std::vector<char>;
    public:
        Parser();
        Parser(std::istream&);
        Parser(std::string);
        std::string NextToken();
        bool MoreTokens();

    private:
        bool isLeftAssoc(const char&);
        int opPriorityDiff(const char&, const char&);
        bool isOperator(const char&);
        bool isParen(const char&);
        bool isOpenParen(const char&);
        bool isCloseParen(const char&);
        std::vector<std::string> split(const std::string&,const char&);
        void tokenize(std::string);
        OpMap operators = {
            {'+',{10,true}},
            {'-',{10,true}},
            {'~',{10,true}}, // Unary minus
            {'*',{20,true}},
            {'/',{20,true}},
            {'%',{20,true}},
            {'^',{40,false}},
            {',',{0,true}}};
        Paren parenOpen = {'(','[','{'};
        Paren parenClose = {')',']','}'};
        Paren parens = {'(','[','{',')',']','}'};
        std::deque<std::string> tokens;
};

#endif
