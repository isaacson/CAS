#ifndef _MATRIX_H_
#define _MATRIX_H_

#include <iostream>
#include <vector>
#include <stdexcept>
#include <algorithm>
#include <set>
#include <stack>
#include <map>
#include "Utility.h"

template<typename T>
class Matrix {
    public:
        class Row {
            using size_type = typename std::vector<T>::size_type;
            private:
                friend class Matrix;
                Row(Matrix& parent, size_type row): m_parent(parent), m_row(row) {}
            public:
                T& operator[](size_type col) {return m_parent.at(m_row,col);}
                bool operator==(Row& other) const {
                    for(size_type i = 0; i < m_parent.cols_; ++i) {
                        if(m_parent.at(m_row,i)!=other[i]) return false;
                    }
                    return true;
                }
                bool operator!=(Row& other) const {
                    return !(*this == other);
                }
            private:
                Matrix& m_parent;
                size_type m_row;
        };

        class Column {
            using size_type = typename std::vector<T>::size_type;
            private:
                friend class Matrix;
                Column(Matrix& parent, size_type col): m_parent(parent), m_col(col) {}
            public:
                bool isZero() {
                    for(size_type i = 0; i < m_parent.rows_; ++i) {
                        if(m_parent.at(i,m_col) != 0) return false;
                    }
                    return true;
                }
                size_type Max() {
                    value_type max = 0;
                    size_type loc = 0;
                    for(size_type i = 0; i < m_parent.rows_; ++i) {
                        if(max < m_parent.at(i,m_col)) {
                            max = m_parent.at(i,m_col);
                            loc = i;
                        }
                    }
                    return loc;
                }
                T& operator[](size_type row) {return m_parent.at(row,m_col);}
                bool operator==(Column& other) const {
                    for(size_type i = 0; i < m_parent.rows_; ++i) {
                        if(m_parent.at(i,m_col)!=other[i]) return false;
                    }
                    return true;
                }
                bool operator!=(Column& other) const {
                    return !(*this == other);
                }
            private:
                Matrix& m_parent;
                size_type m_col;
        };

        using value_type = typename std::vector<T>::value_type;
        using reference = typename std::vector<T>::reference;
        using size_type = typename std::vector<T>::size_type;

        Matrix() : rows_{1}, cols_{1} {
            m_data.resize(1);
        }
        Matrix(size_type const rows, size_type const cols) :
            rows_{rows}, cols_{cols} {
                m_data.resize(rows*cols);
        }
        Matrix(const std::initializer_list<std::initializer_list<value_type>> list) :
            rows_{list.size() }, cols_{list.begin()->size()} {
                m_data.reserve(rows_*cols_);
                for(auto const& row : list) {
                    if(row.size() != cols_)
                        throw std::domain_error("Input data must have same number of columns in each row");
                    m_data.insert(m_data.end(),row);
                }
        }
        Matrix(const std::vector<std::vector<value_type>> list) :
            rows_{list.size()}, cols_{list.begin()->size()} {
                m_data.reserve(rows_*cols_);
                for(auto const& row : list) {
                    if(row.size() != cols_)
                        throw std::domain_error("Input data must have same number of columns in each row");
                    m_data.insert(m_data.end(),row);
                }
        }
        Matrix(size_type const rows, size_type const cols, const std::vector<value_type> list) : 
            rows_{rows}, cols_{cols}, m_data{list} {};
        Matrix(const Matrix&);
        Matrix& operator=(const Matrix&);

        // access operator
        reference operator()(size_type const x, size_type const y) {return m_data[x*cols_+y];}
        const T operator()(size_type const x, size_type const y) const {return m_data[x*cols_+y];}
        Row operator[](size_type const row) {return Row(*this,row);}
        const T at(size_type const x, size_type const y) const {return m_data[x*cols_+y];}
        reference at(size_type const x, size_type const y) {return m_data[x*cols_+y];}

        // assignment operators
        Matrix& operator+=(const Matrix&);
        Matrix& operator-=(const Matrix&);
        Matrix& operator*=(const Matrix&);
        Matrix& operator*=(const double);
        Matrix& operator/=(const double);

        // arithmetic operators
        Matrix operator+(const Matrix&) const;
        Matrix operator-(const Matrix&) const;
        Matrix operator*(const Matrix&) const;
        Matrix operator*(const double) const;
        Matrix operator/(const double) const;
        Matrix operator+() const;
        Matrix operator-() const;

        // comparison operators
        bool operator==(const Matrix&) const;
        bool operator!=(const Matrix& m) const {return !(*this==m);}

        // arithmetic functions
        Matrix pow(unsigned exp) const;
        void swapRows(size_type, size_type);
        Matrix augment(const Matrix);
        Matrix transpose() const;
        Matrix reducedRowEchelon();
        Matrix rowEchelon();
        Matrix inverse();

        size_type rows_;
        size_type cols_;

    private:
        std::vector<T> getRow(size_type i);
        std::vector<T> m_data;
        inline size_type index(size_type const i, size_type const j) const noexcept {
            return i*cols_+j;
        }
};

#include "Matrix.tcc"

#endif
