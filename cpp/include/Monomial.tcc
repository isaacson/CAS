template<typename T>
Monomial<T>::Monomial() : order{0}, coefficient{1}, vars{}, exponents{} {}

template<typename T>
Monomial<T>::Monomial(std::string str) {
    std::vector<std::string> tokens;
    std::istringstream f(str);
    std::string temp;
    while(getline(f,temp,'*')) {
        tokens.push_back(temp);
    }
    init(tokens);
    sort();
    setOrder();
}

template<typename T>
Monomial<T>::Monomial(std::initializer_list<std::string> list) {
    std::vector<std::string> tmp {list};
    init(tmp);
    sort();
    setOrder();
}

template<typename T>
Monomial<T>::Monomial(std::vector<std::string> strings) {
    init(strings); 
    sort();
    setOrder();
}

template<typename T>
void Monomial<T>::init(std::vector<std::string> strings) {
    coefficient = Convert::StringtoT<T>(strings[0]);
    for(auto it = strings.begin()+1, end=strings.end(); it != end; ++it) {
        std::string tmp = *it;
        std::string var; 
        int exp;
        auto pos = tmp.find('^');
        if(pos==std::string::npos) {
            var = tmp; 
            exp = 1;
        } else {
            var = tmp.substr(0,pos);
            exp = std::stoi(tmp.substr(pos+1,tmp.size()));
        }
        if(exponents.find(var) != exponents.end()) {
            exponents[var] += exp;
        } else {
            vars.push_back(var);
            exponents[var] = exp;
        }
    }
}

template<typename T>
Monomial<T>::Monomial(T coeff, std::vector<std::string> vars_, std::vector<int> exps_) : coefficient{coeff}, vars{vars_} {
    if(vars.size() != exps_.size()) throw std::domain_error("Variables and exponents do not line up");
    for(auto i = 0; i < vars.size(); ++i) {
        exponents[vars[i]] = exps_[i];
    }
    sort();
    setOrder();
}
        
template<typename T>
Monomial<T>::Monomial(const Monomial& m) :
    vars{m.vars}, coefficient{m.coefficient}, exponents{m.exponents}, order{m.order} {}

template<typename T>
Monomial<T>& Monomial<T>::operator=(const Monomial<T>& m) {
    vars = m.vars;
    coefficient = m.coefficient;
    exponents = m.exponents;
    order = m.order;
}

template<typename T>
void Monomial<T>::sort() {
    std::sort(vars.begin(),vars.end());
}

template<typename T>
void Monomial<T>::setOrder() {
    order = 0;
    for(auto var : vars) {
        order += exponents[var];
    }
}
        
template<typename T>
bool Monomial<T>::operator==(const Monomial<T>& m) const {
    return exponents == m.exponents;
}

template<typename T>
std::ostream& operator<< (std::ostream& os, const Monomial<T>& m) {
    if(m.coefficient != T{1}) os << m.coefficient << "*";
    for(auto var : m.vars) {
        os << var;
        if(m.exponents.find(var) != m.exponents.end())
            if(m.exponents.at(var) != 1) 
                os << "^" << m.exponents.at(var);
        if(var != *(--m.vars.end()))
            os << "*";
    }
    return os;
}

template<typename T>
Monomial<T>& Monomial<T>::operator+=(const Monomial<T>& m) {
    if(exponents != m.exponents) throw std::domain_error("Trying to add inconsistent Monomials");
    coefficient += m.coefficient;
    return *this;
}

template<typename T>
Monomial<T>& Monomial<T>::operator-=(const Monomial<T>& m) {
    if(exponents != m.exponents) throw std::domain_error("Trying to add inconsistent Monomials");
    coefficient -= m.coefficient;
    return *this;
}

template<typename T>
Monomial<T>& Monomial<T>::operator*=(const Monomial<T>& m) {
    for(auto var : m.vars) {
        if(exponents.find(var) != exponents.end()) {
            exponents[var] += m.exponents.at(var);
        } else {
            vars.push_back(var);
            exponents[var] = m.exponents.at(var);
        }
    }
    coefficient *= m.coefficient;
    setOrder();
    sort();
    return *this;
}

template<typename T>
Monomial<T> Monomial<T>::operator+(const Monomial<T>& m) const {
    return Monomial<T>(*this) += m;
}

template<typename T>
Monomial<T> Monomial<T>::operator-(const Monomial<T>& m) const {
    return Monomial<T>(*this) -= m;
}

template<typename T>
Monomial<T> Monomial<T>::operator*(const Monomial<T>& m) const {
    return Monomial<T>(*this) *= m;
}

template<typename T>
Monomial<T> Monomial<T>::operator+() const {
    return Monomial<T>(*this);
}

template<typename T>
Monomial<T> Monomial<T>::operator-() const {
    Monomial<T> result(*this);
    result.coefficient = -result.coefficient;
    return result;
}
        
template<typename T>
Monomial<T>& Monomial<T>::operator*=(const T& term) {
    coefficient *= term;
    return *this;
}
        
template<typename T>
Monomial<T> Monomial<T>::operator*(const T& term) const {
    return Monomial<T>(*this) *= term;
}
        
template<typename T>
T Monomial<T>::eval(const std::map<std::string,T>& point) const {
    if(point.size() < vars.size()) throw std::runtime_error("Insufficient number arguments to evaluate Monomial");
    T result = coefficient;
    int varCount = 0;
    for(auto var : point) {
        if(exponents.find(var.first) != exponents.end()) {
            result *= std::pow(var.second,exponents.at(var.first));
            ++varCount;
        }
    }
    if(varCount != vars.size()) throw std::runtime_error("Not all variables are defined, and reducing the Monomial is not yet defined");

    return result;
}
