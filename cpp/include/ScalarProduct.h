#ifndef _SCALARPRODUCT_H_
#define _SCALARPRODUCT_H_

#include "ginac/ginac.h"

namespace ginac = GiNaC;

class ScalarProduct : public ginac::basic {
    GINAC_DECLARE_REGISTERED_CLASS(ScalarProduct, ginac::basic);
    public:
        ScalarProduct(const ginac::ex l, const ginac::ex r) : left{l}, right{r} {}
        ginac::ex eval() const override;
        ginac::ex expand(unsigned options) const override;
        size_t nops() const override {return 2;}
        ginac::ex op(size_t i) const override;

    protected:
        void do_print(const ginac::print_context& c, unsigned level = 0) const;

    private:
        ginac::ex left, right;
};

DECLARE_FUNCTION_2P(SP)

#endif
