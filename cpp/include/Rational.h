#ifndef _RATIONAL_H_
#define _RATIONAL_H_

#include <iosfwd>
#include <stdexcept>
#include <type_traits>
#include <utility>
#include "types.h"

class Rational{
    public:
        Rational() : numerator(0), denominator(1) {}
        template<typename T>
        Rational(T numerator_=0, T denominator_ = 1);
        Rational(const Rational& rat);

        // assignment operators
        Rational& operator=(const Rational&);
        Rational& operator+=(const Rational&);
        Rational& operator-=(const Rational&);
        Rational& operator*=(const Rational&);
        Rational& operator/=(const Rational&);

        // arithmetic operators
        Rational operator+(const Rational&) const;
        Rational operator-(const Rational&) const;
        Rational operator*(const Rational&) const;
        Rational operator/(const Rational&) const;
        Rational operator-() const;
        Rational operator+() const;

        // comparison operators
        bool operator==(const Rational&) const;
        bool operator!=(const Rational&) const;
        bool operator<(const Rational&) const;

        // increment and decrement operators
        Rational operator++(int);
        Rational operator--(int);
        Rational& operator++();
        Rational& operator--();

        // type conversion
        explicit operator double();

        // stream operators
        friend std::ostream& operator<< (std::ostream&, const Rational&);
        friend std::istream& operator>> (std::istream&, Rational&);

        // arithmetic functions
        Rational pow(unsigned exp) const;
        Rational inverse() const;

        Int numerator;
        Int denominator;
        Rational& simplify();
};

template<typename T>
Rational::Rational(T p, T q) : numerator{p}, denominator{q} {
    if(denominator == 0)
        throw std::domain_error("Zero Denominator");
    simplify();
}

// Operators with other classes
Rational operator+(Int a, Rational b);
Rational operator-(Int a, Rational b);
Rational operator*(Int a, Rational b);
Rational operator/(Int a, Rational b);
bool operator==(Int a, Rational b);
bool operator!=(Int a, Rational b);
bool operator<(Int a, Rational b);

#endif
