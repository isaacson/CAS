#ifndef _MONOMIAL_H_
#define _MONOMIAL_H_

#include <map>
#include <set>
#include <vector>
#include <string>
#include <sstream>
#include <stdexcept>
#include <iostream>
#include <algorithm>
#include <cmath>
#include "Utility.h"

template<typename T>
class Monomial {
    public:
        // Constructors
        Monomial();
        Monomial(std::string);
        Monomial(std::initializer_list<std::string>);
        Monomial(std::vector<std::string>);
        Monomial(T,std::vector<std::string>,std::vector<int>);
        Monomial(T coeff, std::vector<std::string> vars_, std::map<std::string,int> exps_);
        Monomial(const Monomial&);

        // Assignment operators
        Monomial& operator=(const Monomial&);
        Monomial& operator+=(const Monomial&);
        Monomial& operator-=(const Monomial&);
        Monomial& operator*=(const Monomial&);
        Monomial& operator*=(const T&);
//        Monomial& operator/=(const Monomial&);

        // Arithematic operators
        Monomial operator+(const Monomial&) const;
        Monomial operator-(const Monomial&) const;
        Monomial operator*(const Monomial&) const;
        Monomial operator*(const T&) const;
//        Monomial operator/(const Monomial&) const;
        Monomial operator+() const;
        Monomial operator-() const;
        T operator()(const std::map<std::string,T>& m) const {return eval(m);}

        // Comparison operators
        bool operator==(const Monomial&) const;
        bool operator!=(const Monomial& m) const {return !(*this==m);}

        // Boolean operators
        bool isZero() const {return 0 == coefficient;}
        bool isPos() const {return 0 < coefficient;}
        bool isNeg() const {return coefficient < 0;}
        bool isConstant() const {return vars.empty();}

        // Getters
        T getCoeff() const {return coefficient;}
        T eval(const std::map<std::string,T>&) const;

        // Ordering functions
        friend class lex;
        friend class grlex;
        friend class grevlex;

        // Stream functions
        template<typename U>
        friend std::ostream& operator<< (std::ostream&, const Monomial<U>&);
        template<typename U>
        friend std::istream& operator>> (std::istream&, const Monomial<U>&);

    private:
        void init(std::vector<std::string>);
        void setOrder();
        void sort();

        std::vector<std::string> vars;
        std::map<std::string,int> exponents;
        T coefficient;
        int order;
};

#include "Monomial.tcc"

class compare {
    public:
        std::set<std::string> varList(const std::vector<std::string>&,const std::vector<std::string>&);
        std::vector<int> exponents(const std::map<std::string,int>&,const std::set<std::string>&);
};

class lex : public compare{
    public:
        template<typename T>
        bool operator() (const Monomial<T>& m1, const Monomial<T>& m2) {
            std::set<std::string> vars = varList(m1.vars,m2.vars);
            std::vector<int> e1 = exponents(m1.exponents,vars);
            std::vector<int> e2 = exponents(m2.exponents,vars);
            return std::lexicographical_compare(e1.begin(),e1.end(),e2.begin(),e2.end());
        }
};

class grlex : public compare{
    public:
        template<typename T>
        bool operator() (const Monomial<T>& m1, const Monomial<T>& m2) {
            if(m1.order < m2.order) return true;
            if(m2.order < m1.order) return false;
            std::set<std::string> vars = varList(m1.vars,m2.vars);
            std::vector<int> e1 = exponents(m1.exponents,vars);
            std::vector<int> e2 = exponents(m2.exponents,vars);
            return std::lexicographical_compare(e1.begin(),e1.end(),e2.begin(),e2.end());
        }
};

class grevlex : public compare{
    public:
        template<typename T>
        bool operator() (const Monomial<T>& m1, const Monomial<T>& m2) {
            if(m1.order < m2.order) return true;
            if(m2.order < m1.order) return false;
            std::set<std::string> vars = varList(m1.vars,m2.vars);
            std::vector<int> e1 = exponents(m1.exponents,vars);
            std::vector<int> e2 = exponents(m2.exponents,vars);
            return std::lexicographical_compare(e2.rbegin(),e2.rend(),e1.rbegin(),e1.rend());
        }
};

#endif
