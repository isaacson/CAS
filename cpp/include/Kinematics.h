#ifndef _KINEMATICS_H_
#define _KINEMATICS_H_

#include <vector>
#include <map>
#include <algorithm>
#include <iostream>

#include "ginac/ginac.h"
#include "yaml-cpp/yaml.h"

// Namespace alias to avoid worrying about the capitalization
namespace ginac = GiNaC;

class Crossing;

class Kinematics {
    public:
        Kinematics();
        Kinematics& operator=(const Kinematics&);

        static Kinematics* CreateDerivedKinematics(const std::string&);
        static bool SearchCrossingSuffix(const std::string&, std::string&,
                std::string&,Crossing&);
        static bool SearchDimShiftSuffix(const std::string&, std::string&,
                std::string&, int&);
        static std::string DimShiftedLabel(const std::string&, int);

        // Getters
        const std::string& name() const {return m_name;}
        const ginac::lst& External() const;
        const ginac::lst& Independent() const;
        const ginac::lst& Incoming() const;
        const ginac::lst& Outgoing() const;
        const ginac::lst& Invariants() const;
        const ginac::ex& Dimension() const;
        const ginac::lst& AllSymbols() const;
        ginac::lst ExternalMomentaAndKinematics() const;
        const std::string LoopPrefix() const;
        const ginac::scalar_products& MomentaToInvariants() const {
            return momenta_invariants;
        }
        const ginac::exmap& RulesSPtoInvariants() const {
            return rules_sp_invariants;
        }
        const Crossing& crossing(const std::string& name) const;
        const ginac::exmap& RuleMomentumConservation() const {
            return rule_momentum_conservation;
        }
        const ginac::symbol* SymbolToReplaceByOne() const {
            if(symbol_replace_one.get_name() == "undefined")
                return 0;
            return &symbol_replace_one;
        }
        ginac::exmap GetRuleSymbolToReplaceByOne() const {
            ginac::exmap rule;
            if(symbol_replace_one.get_name() != "undefined")
                rule[symbol_replace_one] = 1;
            return rule;
        }
        int FindMassDim(const ginac::ex&) const;

        // Setters
        void addIncoming(const std::string&);
        void addOutgoing(const std::string&);
        void addExternal(const std::string&);
        void addInvariant(const std::string&, const int&);
        void addSPRule(const std::string&, const std::string&, const std::string&, ginac::lst&);
        void setSymbolReplaceOne(const std::string&);
        void setMomentumConservation(const std::string&, const std::string&);
        void SetID(int id);
        void setExternal(const ginac::lst&);
        void setIndependent(const ginac::lst&);
        void setIncoming(const ginac::lst&);
        void setOutgoing(const ginac::lst&);
        void setInvariants(const ginac::lst&);
        void setSPRules(const ginac::lst&);
        void setName(const std::string&);

        // Compare Kinematics
        int id() const;
        bool operator<(const Kinematics& other) const;
        bool operator==(const Kinematics& other) const;
        bool operator!=(const Kinematics& other) const;

        std::vector<std::string> GetOpts() const {return opts;}

        friend std::ostream& operator<< (std::ostream&, const Kinematics&);
    private:
        std::string m_name;
        ginac::lst external, independent;
        ginac::lst incoming, outgoing;
        ginac::lst invariants;
        ginac::lst all_symbols;
        ginac::ex dimension;
        ginac::symbol symbol_replace_one;
        std::map<ginac::symbol, int, ginac::ex_is_less> mass_dimension;
        ginac::exmap rules_sp_invariants;
        ginac::scalar_products momenta_invariants;
        ginac::exmap rule_momentum_conservation;

        std::string loop_prefix;

        int m_id;
        const std::vector<std::string> opts{"incoming_momenta","outgoing_momenta",
            "momentum_conservation","kinematic_invariants","scalarproduct_rules",
            "symbol_to_replace_by_one"};

};

namespace YAML {

template<>
struct convert<Kinematics> {
    static Node encode(const Kinematics& rhs) {
        Node node;
    }
    static bool decode(const Node& node, Kinematics& rhs) {
        for(auto elm : node) {
            std::vector<std::string> opts = rhs.GetOpts();
            auto token = elm.first.as<std::string>();
            if(std::find(opts.begin(),opts.end(),token) == opts.end())
                throw std::runtime_error("Option: " + token + " not recongnized in kinematics.yaml");
        }

        if(node["incoming_momenta"]) {
            for(auto elm : node["incoming_momenta"]) {
                rhs.addIncoming(elm.as<std::string>());
                rhs.addExternal(elm.as<std::string>());
            }
        }

        if(node["outgoing_momenta"]) {
            for(auto elm : node["outgoing_momenta"]) {
                rhs.addOutgoing(elm.as<std::string>());
                rhs.addExternal(elm.as<std::string>());
            }
        }

        if(rhs.External().nops() > 0) {
            if(node["momentum_conservation"]) {
                const Node& n = node["momentum_conservation"];
                if(n.size()==2) 
                    rhs.setMomentumConservation(n[0].as<std::string>(),n[1].as<std::string>());
                else
                    throw std::runtime_error("Invalid momentum conservation rules");
            }
        }

        if(node["kinematic_invariants"]) {
            for(auto invariant : node["kinematic_invariants"]) {
                rhs.addInvariant(invariant[0].as<std::string>(),invariant[1].as<int>());
            }
        }

        if(node["scalarproduct_rules"]) {
            ginac::lst eqns;
            for(auto rules : node["scalarproduct_rules"]) {
                if(rules[0].size() != 2)
                    throw std::runtime_error("Scalar product requires a list of 2 momenta");
                rhs.addSPRule(
                            rules[0][0].as<std::string>(),
                            rules[0][1].as<std::string>(),
                            rules[1].as<std::string>(),
                            eqns);
            }
            rhs.setSPRules(eqns);
        }

        if(node["symbol_to_replace_by_one"]) {
            if(node["symbol_to_replace_by_one"].IsNull()) 
                rhs.setSymbolReplaceOne("undefined");
            else
                rhs.setSymbolReplaceOne(node["symbol_to_replace_by_one"].as<std::string>());
        }

        return true;
    }
};
}

#endif
