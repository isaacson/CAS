#ifndef _MODN_H_
#define _MODN_H_

#include <iostream>
#include <exception>
#include <type_traits>
#include <utility>
#include <vector>
#include "types.h"
#include "Rational.h"

// Used for known at compile time Zp fields
template<Int N>
class Mod{
    public:
        Mod(Int value);
        Mod(Rational value);
        Mod(const Mod& mod);

        // assignment operators
        Mod& operator=(const Mod&);
        Mod& operator+=(const Mod&);
        Mod& operator-=(const Mod&);
        Mod& operator*=(const Mod&);
        Mod& operator/=(const Mod&);

        // arithmetic operatos
        Mod operator+(const Mod&) const;
        Mod operator-(const Mod&) const;
        Mod operator*(const Mod&) const;
        Mod operator/(const Mod&) const;
        Mod operator+() const;
        Mod operator-() const;

        // comparison operators
        bool operator==(const Mod&) const;
        bool operator<(const Mod&) const;

        // increment and decrement operators
        Mod operator++(int);
        Mod operator--(int);
        Mod& operator++();
        Mod& operator--();

        // type conversion
        explicit operator Int();

        // stream operators
        template<Int M>
        friend std::ostream& operator<< (std::ostream&, const Mod<M>&);
        //template<Int M>
        //friend std::istream& operator>> (std::istream&, ModN<M>&);

        // arithmetic functions
        Mod pow(unsigned exp) const;
        Mod inverse() const;

    private:
        Int value;
};

#include "ModN.tcc"

class ModN{
    public:
        ModN(Int N = 2);
        ModN(Int value, Int N);
        ModN(Rational value, Int N);
        ModN(const ModN& mod);

        // assignment operators
        ModN& operator=(const ModN&);
        ModN& operator+=(const ModN&);
        ModN& operator-=(const ModN&);
        ModN& operator*=(const ModN&);
        ModN& operator/=(const ModN&);

        // arithmetic operatos
        ModN operator+(const ModN&) const;
        ModN operator-(const ModN&) const;
        ModN operator*(const ModN&) const;
        ModN operator/(const ModN&) const;
        ModN operator+() const;
        ModN operator-() const;

        // comparison operators
        bool operator==(const ModN&) const;
        bool operator<(const ModN&) const;

        // increment and decrement operators
        ModN operator++(int);
        ModN operator--(int);
        ModN& operator++();
        ModN& operator--();

        // type conversion
        explicit operator Int() {return value;}
        explicit operator Int() const {return value;}

        // stream operators
        friend std::ostream& operator<< (std::ostream&, const ModN&);
        friend std::istream& operator>> (std::istream&, ModN&);

        // arithmetic functions
        ModN pow(unsigned exp) const;
        ModN inverse() const;

        // Getters
        Int GetMod() const {return N;}

        // Setters
        void SetValue(Int v) {value = v;}
        void SetValue(Rational v);

    private:
        Int value;
        Int N;
};

ModN ChineseRemainder(std::vector<ModN> vec);

// Operators with other classes
ModN operator+(Int a, ModN b);
ModN operator-(Int a, ModN b);
ModN operator*(Int a, ModN b);
ModN operator/(Int a, ModN b);
bool operator==(Int a, ModN b);
bool operator<(Int a, ModN b);
ModN operator+(Rational a, ModN b);
ModN operator-(Rational a, ModN b);
ModN operator*(Rational a, ModN b);
ModN operator/(Rational a, ModN b);
bool operator==(Rational a, ModN b);
bool operator<(Rational a, ModN b);

#endif
