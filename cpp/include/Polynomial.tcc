template<typename T, typename F>
Polynomial<T,F>::Polynomial() : monomials{} {}

template<typename T, typename F>
Polynomial<T,F>::Polynomial(std::string expr) : monomials{} {
    Parser p(expr);
    std::string monString;
    std::string tmp;
    while(p.MoreTokens()) {
        tmp = p.NextToken();
        if(tmp == "-" || tmp == "+") {
            Monomial<T> monTmp(monString);
            auto it = std::find(monomials.begin(),monomials.end(),monTmp);
            if(it != monomials.end())
                *it += monTmp;
            else monomials.push_back(monTmp);
            monString = tmp;
        } else {
            if(tmp == "~") monString += "-";
            else monString += tmp;
        }
    }
    Monomial<T> monTmp(monString);
    auto it = std::find(monomials.begin(),monomials.end(),monTmp);
    if(it != monomials.end())
        *it += monTmp;
    else monomials.push_back(monTmp);
    std::sort(monomials.begin(),monomials.end(), sortAlg);
}
        
template<typename T, typename F>
Polynomial<T,F>::Polynomial(std::vector<Monomial<T>> m) : monomials{m} {}

template<typename T, typename F>
Polynomial<T,F>& Polynomial<T,F>::operator=(const Polynomial& p) {
    monomials = p.monomials;
}

template<typename T, typename F>
Polynomial<T,F>& Polynomial<T,F>::operator+=(const Polynomial& p) {
    std::vector<Monomial<T>> result; 
    auto lBegin = monomials.begin();
    auto rBegin = p.monomials.begin();
    auto lEnd = monomials.end();
    auto rEnd = p.monomials.end();

    while(lBegin != lEnd && rBegin != rEnd) {
        if(sortAlg(*lBegin,*rBegin)) {
            result.push_back(*lBegin++);
        } else if(sortAlg(*rBegin,*lBegin)) {
            result.push_back(*rBegin++);
        } else {
            result.push_back(*lBegin+*rBegin);
            ++lBegin;
            ++rBegin;
        }
    }

    while(lBegin != lEnd) result.push_back(*lBegin++);
    while(rBegin != rEnd) result.push_back(*rBegin++);

    monomials = result;
    Simplify();

    return *this;
}

template<typename T, typename F>
Polynomial<T,F>& Polynomial<T,F>::operator-=(const Polynomial& p) {
    std::vector<Monomial<T>> result; 
    auto lBegin = monomials.begin();
    auto rBegin = p.monomials.begin();
    auto lEnd = monomials.end();
    auto rEnd = p.monomials.end();

    while(lBegin != lEnd && rBegin != rEnd) {
        if(sortAlg(*lBegin,*rBegin)) {
            result.push_back(*lBegin++);
        } else if(sortAlg(*rBegin,*lBegin)) {
            result.push_back(-*rBegin++);
        } else {
            result.push_back(*lBegin-*rBegin);
            ++lBegin;
            ++rBegin;
        }
    }

    while(lBegin != lEnd) result.push_back(*lBegin++);
    while(rBegin != rEnd) result.push_back(-*rBegin++);

    monomials = result;
    Simplify();

    return *this;
}

template<typename T, typename F>
Polynomial<T,F>& Polynomial<T,F>::operator*=(const Polynomial& p) {
    std::vector<Monomial<T>> result;
    for(auto mon1 : monomials) {
        for(auto mon2 : p.monomials) {
            Monomial<T> tmp = mon1*mon2;
            auto it = std::find(result.begin(),result.end(),tmp);
            if(it!=result.end()) {
                *it += tmp;
            } else {
                result.push_back(tmp);
            }
        }
    }

    monomials = result;
    std::sort(monomials.begin(),monomials.end(), sortAlg);

    return *this;
}

template<typename T, typename F>
Polynomial<T,F>& Polynomial<T,F>::operator+=(const Monomial<T>& monomial) {
    auto it = std::find(monomials.begin(),monomials.end(),monomial);
    if(it != monomials.end()) {
        *it += monomial;
        Simplify();
    } else {
        monomials.push_back(monomial);
        std::sort(monomials.begin(),monomials.end(), sortAlg);
    }
    return *this;
}

template<typename T, typename F>
Polynomial<T,F>& Polynomial<T,F>::operator-=(const Monomial<T>& monomial) {
    auto it = std::find(monomials.begin(),monomials.end(),monomial);
    if(it != monomials.end()) {
        *it -= monomial;
        Simplify();
    } else {
        monomials.push_back(-monomial);
        std::sort(monomials.begin(),monomials.end(), sortAlg);
    }
    return *this;
}

template<typename T, typename F>
Polynomial<T,F>& Polynomial<T,F>::operator*=(const Monomial<T>& monomial) {
    Monomials monomialsNew;
    for(auto mon : monomials) {
        monomialsNew.insert(mon*monomial);
    }
    monomials = monomialsNew;

    return *this;
}

template<typename T, typename F>
Polynomial<T,F>& Polynomial<T,F>::operator+=(const T& term) {
    if(monomials[0].isConstant()) monomials[0] += term;
    else monomials.insert(monomials.begin(),term);
    Simplify();
    return *this;
}

template<typename T, typename F>
Polynomial<T,F>& Polynomial<T,F>::operator-=(const T& term) {
    if(monomials[0].isConstant()) monomials[0] -= term;
    else monomials.insert(monomials.begin(),-term);
    Simplify();
    return *this;
}

template<typename T, typename F>
Polynomial<T,F>& Polynomial<T,F>::operator*=(const T& term) {
    for(auto mon : monomials) {
        mon = term*mon;
    }
    return *this;
}

template<typename T, typename F>
Polynomial<T,F> Polynomial<T,F>::operator+(const Polynomial& p) const {
    return Polynomial<T,F>(*this) += p;
}

template<typename T, typename F>
Polynomial<T,F> Polynomial<T,F>::operator-(const Polynomial& p) const {
    return Polynomial<T,F>(*this) -= p;
}

template<typename T, typename F>
Polynomial<T,F> Polynomial<T,F>::operator*(const Polynomial& p) const {
    return Polynomial<T,F>(*this) *= p;
}

template<typename T, typename F>
Polynomial<T,F> Polynomial<T,F>::operator+(const Monomial<T>& m) const {
    return Polynomial<T,F>(*this) += m;
}

template<typename T, typename F>
Polynomial<T,F> Polynomial<T,F>::operator-(const Monomial<T>& m) const {
    return Polynomial<T,F>(*this) -= m;
}

template<typename T, typename F>
Polynomial<T,F> Polynomial<T,F>::operator*(const Monomial<T>& m) const {
    return Polynomial<T,F>(*this) *= m;
}

template<typename T, typename F>
Polynomial<T,F> Polynomial<T,F>::operator+(const T& term) const {
    return Polynomial<T,F>(*this) += term;
}

template<typename T, typename F>
Polynomial<T,F> Polynomial<T,F>::operator-(const T& term) const {
    return Polynomial<T,F>(*this) -= term;
}

template<typename T, typename F>
Polynomial<T,F> Polynomial<T,F>::operator*(const T& term) const {
    return Polynomial<T,F>(*this) *= term;
}

template<typename T, typename F>
Polynomial<T,F> Polynomial<T,F>::operator+() const {
    return *this;
}

template<typename T, typename F>
Polynomial<T,F> Polynomial<T,F>::operator-() const {
    Monomials newMon(monomials.size());
    for(auto it = monomials.begin(); it != monomials.end(); ++it) {
        newMon.push_back(-*it);
    }

    return Polynomial<T,F>(newMon);
}

template<typename T, typename F>
bool Polynomial<T,F>::isZero() const {
    return monomials.size() == 0;
}

template<typename T, typename F>
bool Polynomial<T,F>::isConstant() const {
    return monomials.size() == 1 && monomials[0].isConstant();
}

template<typename T, typename F>
bool Polynomial<T,F>::operator== (const Polynomial<T,F>& p) const {
    if(monomials!=p.monomials) return false;
    for(size_t i = 0; i < monomials.size(); ++i) {
        if(monomials[i].getCoeff() != p.monomials[i].getCoeff()) return false;
    }
    return true;
}

template<typename T, typename F>
T Polynomial<T,F>::eval(const std::map<std::string,T>& m) const {
    T result; 
    for(auto mon : monomials) result += mon.eval(m);
    return result;
}


template<typename T, typename F>
std::ostream& operator<< (std::ostream& os, const Polynomial<T,F>& p) {
    for(auto mon = p.monomials.rbegin(), end = p.monomials.rend();
            mon != end; ++mon) {
        if(mon != p.monomials.rbegin()) {
            if(mon->isPos()) os << "+";
        }
        os << *mon;
    }
    return os;
}

template<typename T, typename F>
std::istream& operator>> (std::istream& is, Polynomial<T,F>& p) {
    std::string expr = std::string(std::istreambuf_iterator<char>(is),{});
    p = Polynomial<T,F>(expr);
    return is;
}
        
