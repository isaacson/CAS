#ifndef _TOOLS_H_
#define _TOOLS_H_

#include <map>

#include "ginac/ginac.h"

namespace ginac = GiNaC;

const ginac::symbol& get_symbol(const std::string& str) {
    static std::map<std::string, ginac::symbol> symbol_map;
    auto it = symbol_map.find(str);
    if(it != symbol_map.end()) return it -> second;
    else return symbol_map.insert(std::make_pair(str,ginac::symbol(str))).first -> second;
}

#endif
