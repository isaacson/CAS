#ifndef _POLYNOMIALS_H_
#define _POLYNOMIALS_H_

#include <map>
#include <set>
#include <vector>
#include <string>
#include <sstream>
#include <stdexcept>
#include <iostream>
#include <algorithm>
#include <functional>
#include "Monomial.h"
#include "Parser.h"


template<typename T, typename F>
class Polynomial {
    using Monomials = typename std::vector<Monomial<T>>;
    using MonomialsIt = typename Monomials::iterator;
    using MonomialsCit = typename Monomials::const_iterator;
    using MonomialsRet = typename std::pair<MonomialsIt,bool>;
    using MonomialsBackIt = typename std::back_insert_iterator<Monomials>;
    public:
        // Constructors
        Polynomial();
        Polynomial(std::string);
        Polynomial(std::initializer_list<std::string>);
        Polynomial(std::vector<std::string>);
        Polynomial(std::vector<Monomial<T>>);
        Polynomial(Monomial<T>);

        // Assignment operators
        Polynomial& operator=(const Polynomial&);
        Polynomial& operator+=(const Polynomial&);
        Polynomial& operator-=(const Polynomial&);
        Polynomial& operator*=(const Polynomial&);
        Polynomial& operator+=(const Monomial<T>&);
        Polynomial& operator-=(const Monomial<T>&);
        Polynomial& operator*=(const Monomial<T>&);
        Polynomial& operator+=(const T&);
        Polynomial& operator-=(const T&);
        Polynomial& operator*=(const T&);

        // Arithematic operators
        Polynomial operator+(const Polynomial&) const;
        Polynomial operator-(const Polynomial&) const;
        Polynomial operator*(const Polynomial&) const;
        Polynomial operator+(const Monomial<T>&) const;
        Polynomial operator-(const Monomial<T>&) const;
        Polynomial operator*(const Monomial<T>&) const;
        Polynomial operator+(const T&) const;
        Polynomial operator-(const T&) const;
        Polynomial operator*(const T&) const;
        Polynomial operator+() const;
        Polynomial operator-() const;
        T operator()(const std::map<std::string,T>& m) const {return eval(m);}
        T eval(const std::map<std::string,T>&) const;
        void Simplify() {monomials.erase(std::remove_if(monomials.begin(),monomials.end(),
                [](const Monomial<T> &m) {return m.isZero();}),monomials.end());}

        // Comparison operators
        bool operator==(const Polynomial&) const;
        bool operator!=(const Polynomial& p) const {return !(*this==p);}
        bool isZero() const;
        bool isConstant() const;

        // Stream functions
        template<typename U, typename G>
        friend std::ostream& operator<< (std::ostream&, const Polynomial<U,G>&);
        template<typename U, typename G>
        friend std::istream& operator>> (std::istream&, const Polynomial<U,G>&);

    private:
        Monomials monomials;
        F sortAlg;
};

#include "Polynomial.tcc"

#endif
